//
//  BaseViewController.m
//  PDOSystem
//
//  Created by apple on 2017/5/8.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    
    
    
    self.navigationController.navigationBarHidden = NO;
    
    //状态栏颜色
//    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, 20)];
//    statusBarView.backgroundColor = RGB(6, 6, 6, 1);
//    [self.view addSubview:statusBarView];
    //状态条
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, 20)];
    statusBarView.backgroundColor = RGB(6, 6, 6, 1);
    [[[[UIApplication sharedApplication]delegate]window] addSubview:statusBarView];
    
    
    [self addNavigatorBar];
    self.view.backgroundColor = BACKGROUND_COLOR;
    
    //取消键盘
    [self dismissKeyBorder];
    //弹出键盘上移动
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyBoardHasAppear:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyBoardHideAppear) name:UIKeyboardDidHideNotification object:nil];
    
}



- (void)dismissKeyBorder{
     UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
     tapGestureRecognizer.cancelsTouchesInView = NO;
    
      [self.view addGestureRecognizer:tapGestureRecognizer];
}

-(void)keyboardHide:(UITapGestureRecognizer*)tap{
    [self.view endEditing:YES];
}



- (void)keyBoardHasAppear:(NSNotification *)userInfo{
    
    id firstResponder = [self findFirstResponser];
    //    if ( [firstResponder isEqual:_hitch_des]) {
    CGRect keyRect = [[userInfo.userInfo objectForKey:@"UIKeyboardFrameEndUserInfoKey"]CGRectValue];
    [UIView animateWithDuration:0.3 animations:^{
        float offset = [self isOffsetFrameWithSubView:firstResponder SuperView:self.view keyBoard:keyRect];
        if (offset < 0) {
            CGFloat y = _bgScrollView.center.y;
            y = y + offset;
            CGPoint point = CGPointMake(_bgScrollView.center.x, y);
            _bgScrollView.center = point;
        }
    }];
    //    }
    
    
}
- (void)keyBoardHideAppear{
    [UIView animateWithDuration:0.3 animations:^{
        _bgScrollView.frame = CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT);
    }];
}



- (void)addNavigatorBar{
    
    self.title = _barTitle;
    
    
    NSDictionary *dict = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:17]};
    self.navigationController.navigationBar.titleTextAttributes = dict;
    
    [self.navigationController.navigationBar setBarTintColor:RGB(231, 0, 18, 1)];
    

    //leftBtn
    _leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 16, 16)];
    [_leftBtn setBackgroundImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [_leftBtn addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBar = [[UIBarButtonItem alloc]initWithCustomView:_leftBtn];
    self.navigationItem.leftBarButtonItem = leftBar;
    

    


    
}
- (void)backBtn{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
