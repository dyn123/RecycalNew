//
//  CustomBtnForTableCell.h
//  PDOSystem
//
//  Created by apple on 2017/5/25.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomBtnForTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
