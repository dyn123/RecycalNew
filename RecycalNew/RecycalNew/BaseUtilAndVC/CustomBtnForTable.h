//
//  CustomBtnForTable.h
//  Address
//
//  Created by apple on 2017/5/10.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomBtnForTable : UIView

@property (nonatomic,strong)IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (nonatomic,strong)UITableView *tabView;


@property (nonatomic,assign)BOOL showList;//是否展开

@property (nonatomic,strong)NSArray *listArr;//展开的数据

@end
