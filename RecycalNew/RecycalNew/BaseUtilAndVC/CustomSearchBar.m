//
//  CustomSearchBar.m
//  PDOSystem
//
//  Created by apple on 2017/5/10.
//  Copyright © 2017年 apple. All rights reserved.
//

//参考链接   http://www.jianshu.com/p/c1a51bee474c


#import "CustomSearchBar.h"

@implementation CustomSearchBar


- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
      //  [self viewController].navigationController.navigationBar.translucent = YES;
      //  self.showsCancelButton = YES;
        self.placeholder = @"请输入搜索内容";
        UITextField *tf = [self valueForKey:@"searchField"];
        if (tf) {
            //自定义放大镜
            UIImage *image = [UIImage imageNamed: @"mainSearch_icon"];
            UIImageView *iView = [[UIImageView alloc] initWithImage:image];
            
           
            iView.frame = CGRectMake(0, 0, 13,13);
            tf.leftView  = iView;
            
            
        
            //修改placeholder字体颜色
            [tf setValue:RGB(246, 171, 169, 1)forKeyPath:@"_placeholderLabel.textColor"];
            [tf setValue:[UIFont boldSystemFontOfSize:12] forKeyPath:@"_placeholderLabel.font"];
            //背景颜色
            UIColor *bgColor = RGB(205, 1, 17, 1);
            [tf setBackgroundColor:bgColor];
            
            
            CGRect rect = tf.frame;
            rect.size.width = 24;
            tf.frame = rect;
            
            tf.layer.cornerRadius  = 5.0f;
            tf.layer.borderColor   = bgColor.CGColor;
            tf.layer.borderWidth   = 1;
            tf.layer.masksToBounds = YES;
            //光标颜色
            [tf setTintColor:[UIColor blueColor]];
            
            self.backgroundImage = [[UIImage alloc] init];
            
        }
        
        
    }
    return self;
}


- (UIImage*) GetImageWithColor:(UIColor*)color andHeight:(CGFloat)height
{
    CGRect r= CGRectMake(0.0f, 0.0f, 1.0f, height);
    UIGraphicsBeginImageContext(r.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, r);
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}



@end
