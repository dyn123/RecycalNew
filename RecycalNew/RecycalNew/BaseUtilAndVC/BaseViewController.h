//
//  BaseViewController.h
//  PDOSystem
//
//  Created by apple on 2017/5/8.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@property (nonatomic,strong)UIButton *leftBtn;
@property (nonatomic,strong)NSString *barTitle;

@property (nonatomic,strong)UIScrollView *bgScrollView;

@end
