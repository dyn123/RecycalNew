//
//  CustomBtnForTable.m
//  Address
//
//  Created by apple on 2017/5/10.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "CustomBtnForTable.h"
#import "CustomBtnForTableCell.h"
@interface CustomBtnForTable()<UITableViewDelegate,UITableViewDataSource>
{
    UIView *_bgView;
}
@end
@implementation CustomBtnForTable

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}
- (void)setup{
    [[NSBundle mainBundle] loadNibNamed:@"CustomBtnForTable" owner:self options:nil];
    [self addSubview:self.view];
    self.view.frame = self.bounds;
    
}

- (IBAction)btnClick:(id)sender {
    
    [UIView transitionWithView:self duration:0.4f options:UIViewAnimationOptionTransitionNone animations:^{
        if(!self.showList){
            self.showList = YES;
            
        }else{
            
            self.showList = NO;
           
        }

        
    } completion:NULL];
}

- (void)setShowList:(BOOL)showList{
    if (showList == NO) {
        _showList = NO;
        
        [_tabView removeFromSuperview];
        [_bgView removeFromSuperview];
    }else{
        _showList = YES;
        UIWindow * window=WINDOW;
        
        CGFloat H = 192;
        
        CGRect btnRect = [_button convertRect:_button.bounds toView:window];
        NSLog(@"======btnRect===%f===%f===%f===%f",btnRect.origin.x,btnRect.origin.y,btnRect.size.width,btnRect.size.height);
        CGRect rect = CGRectMake(0, btnRect.origin.y+btnRect.size.height, DEVICE_WIDTH , H);
        CGRect superRect =[self convertRect:rect toView:self.superview];
        NSLog(@"======superRect===%f===%f===%f===%f",superRect.origin.x,superRect.origin.y,superRect.size.width,superRect.size.height);
        superRect.origin.x = 0;
        
        _tabView = [[UITableView alloc]initWithFrame:superRect style:UITableViewStyleGrouped];
        _tabView.backgroundColor = [UIColor orangeColor];
        _tabView.delegate = self;
        _tabView.dataSource = self;
        _tabView.tableHeaderView =[[UITableViewHeaderFooterView alloc]initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, 0.1)];
        
        
        CGRect superRectWindow = superRect;
        superRectWindow.size.height= DEVICE_HEIGHT;
        _bgView = [[UIView alloc]initWithFrame:superRectWindow];
        _bgView.backgroundColor = RGB(191, 191, 191, 0.5);
        [window addSubview:_bgView];
        
        
        [window addSubview:_tabView];
    }
}


#pragma mark - tableData
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _listArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 64;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CustomBtnForTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell =[[[NSBundle mainBundle]loadNibNamed:@"CustomBtnForTableCell" owner:nil options:nil]lastObject];
        
    }
    cell.label.text = _listArr[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"点击了单元格");
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    UIView *result = [super hitTest:point withEvent:event];
    
    CGPoint buttonPoint = [_tabView convertPoint:point fromView:self];
    
    if ([_tabView pointInside:buttonPoint withEvent:event]) {
        
        return _tabView;
        
    }
    
    return result;
    
}

@end
