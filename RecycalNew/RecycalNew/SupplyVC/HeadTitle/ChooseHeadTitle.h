//
//  GoodsHeadTitle.h
//  PDOSystem
//
//  Created by apple on 2017/5/10.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBtnForTable.h"
@interface ChooseHeadTitle : UIView
@property (weak, nonatomic) IBOutlet CustomBtnForTable *btnFirst;
@property (weak, nonatomic) IBOutlet CustomBtnForTable *btnSecond;
@property (weak, nonatomic) IBOutlet CustomBtnForTable *btnThird;
@property (nonatomic,strong)NSArray *titleArr;
@property (nonatomic,strong)NSArray *dataArr;
@end
