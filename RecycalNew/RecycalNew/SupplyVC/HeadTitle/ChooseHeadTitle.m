//
//  GoodsHeadTitle.m
//  PDOSystem
//
//  Created by apple on 2017/5/10.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "ChooseHeadTitle.h"
#import "CustomBtnForTable.h"

@interface ChooseHeadTitle()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *width;

@end
@implementation ChooseHeadTitle
- (void)awakeFromNib{
    [super awakeFromNib];
    if (IS_IPHONE5 || IS_IPHONE4) {
        
        _width.constant = 70;
    }else{
        _width.constant = 80;
    }
    
}

/*
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    UIView *result = [super hitTest:point withEvent:event];
    CustomBtnForTable *tabFirst = (CustomBtnForTable *)_btnFirst;
    
    CGPoint buttonPoint = [tabFirst.tabView convertPoint:point fromView:self];
    
    if ([tabFirst.tabView pointInside:buttonPoint withEvent:event]) {
        
        return tabFirst.tabView;
        
    }
    
    return result;
    
}
*/
- (void)setTitleArr:(NSArray *)titleArr{
    
    [_btnFirst.button setTitle:titleArr[0] forState:UIControlStateNormal];
    [_btnSecond.button setTitle:titleArr[1] forState:UIControlStateNormal];
    [_btnThird.button setTitle:titleArr[2] forState:UIControlStateNormal];

    
}
- (void)setDataArr:(NSArray *)dataArr{
    _btnFirst.listArr = dataArr[0];
    _btnSecond.listArr = dataArr[1];
    _btnThird.listArr = dataArr[2];
    
}

@end
