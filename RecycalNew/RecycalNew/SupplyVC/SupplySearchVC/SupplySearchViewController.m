//
//  SupplySearchViewController.m
//  RecycalNew
//
//  Created by apple on 2017/8/5.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "SupplySearchViewController.h"
#import "ChooseHeadTitle.h" //下拉选择
#import "SearchBarHeadView.h"

#import "MyCollectTableViewCell.h"
#define HEADTITLE_HEIGHT 48
@interface SupplySearchViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *redLine;
@property (nonatomic,strong)UIButton *selectBtn;
@property (nonatomic,strong)UIScrollView *bgScrol;

@end

@implementation SupplySearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.navigationBar.hidden = YES;
    [self searchBar];
    
    
    
    _bgScrol = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64+40, DEVICE_WIDTH, DEVICE_HEIGHT)];
    [self.view addSubview:_bgScrol];
    _bgScrol.contentSize = CGSizeMake(DEVICE_WIDTH*3, 0);
    _bgScrol.scrollEnabled = NO;
    
//    [self downChooseView];
    
    
    [self createTableView];
    _selectBtn = [self.view viewWithTag:100];

}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}


- (void)createTableView{
    
    
    float f=48+5;
    float height = DEVICE_HEIGHT-64-40-48-5;
    for (int i=0; i<2; i++) {
        
        UITableView *tabView = [[UITableView alloc]initWithFrame:CGRectMake(DEVICE_WIDTH*i, f, DEVICE_WIDTH, height) style:UITableViewStyleGrouped];
        [_bgScrol addSubview:tabView];
        tabView.backgroundColor =RGB(242, 244, 245, 1);
        tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tabView.delegate = self;
        tabView.dataSource = self;
    }
    
    
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

#pragma mark - tabData
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPHONE4||IS_IPHONE5) {
        return 75;
        
    }else{
        return 95;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MyCollectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell =[[[NSBundle mainBundle] loadNibNamed:@"MyCollectTableViewCell" owner:self options:nil]lastObject];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
        cell.flag = @"myLock_lock";
    }
    return cell;
    
}






- (void)downChooseView{
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 109, DEVICE_WIDTH, 48)];
    [self.view addSubview:v];
    
    NSArray *title = @[@"所在仓库",@"所属分类",@"入仓时间"];
    NSArray *dataArr = @[@[@"仓库1",@"仓库2",@"仓库3"],@[@"二手设备",@"回收机床",@"废铁"],@[@"一个月",@"两个月",@"一年"]];
    ChooseHeadTitle *headTitle = [[[NSBundle mainBundle]loadNibNamed:@"ChooseHeadTitle" owner:nil options:nil]lastObject];
    headTitle.frame = CGRectMake(0, 0, DEVICE_WIDTH, HEADTITLE_HEIGHT);
    [v addSubview:headTitle];
    headTitle.titleArr = title;
    headTitle.dataArr = dataArr;
    
    
}

- (void)searchBar{
    
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 20, DEVICE_WIDTH, 44)];
    [self.view addSubview:v];
    SearchBarHeadView *searchView = [[[NSBundle mainBundle]loadNibNamed:@"SearchBarHeadView" owner:self options:nil]lastObject];
    searchView.titleArr = @[@"供应"];
    searchView.dataArr = @[@"供求",@"资产",@"拍卖",@"租赁"];
    searchView.frame = CGRectMake(0, 0, DEVICE_WIDTH, v.frame.size.height);
    [v addSubview:searchView];
    
}
- (IBAction)btnClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    
    int tag = (int)btn.tag - 100;
    _selectBtn.selected = NO;
    btn.selected = YES;
    _selectBtn =  btn;
    //询价留言
    [UIView animateWithDuration:0.5 animations:^{
        _bgScrol.contentOffset = CGPointMake(DEVICE_WIDTH * tag, 0);
        
        float l = DEVICE_WIDTH/2;
        _redLine.center = CGPointMake(l/2+l*tag , _redLine.center.y);
    } completion:nil];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
