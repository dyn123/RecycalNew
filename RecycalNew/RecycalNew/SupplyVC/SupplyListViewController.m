//
//  SupplyListViewController.m
//  RecycalNew
//
//  Created by apple on 2017/8/4.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "SupplyListViewController.h"

#import "MyCollectTableViewCell.h"

//#import "ChooseHeadTitle.h" //下拉选择


#import "SupplySearchViewController.h"
#import "SupplyInfoViewController.h"

#import "GQinfoListModel.h"


#define HEADTITLE_HEIGHT 48
@interface SupplyListViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *redLine;
@property (nonatomic,strong)UIButton *selectBtn;
@property (nonatomic,strong)UIScrollView *bgScrol;

@property (nonatomic,strong)NSMutableArray *objLeftArr;

@property (nonatomic,strong)UITableView *tabView;
@end

@implementation SupplyListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = @"供求信息";
    self.tabBarController.tabBar.hidden = YES;
    
    
    [self navInfo];
    
    
    
    _bgScrol = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64+40, DEVICE_WIDTH, DEVICE_HEIGHT)];
    [self.view addSubview:_bgScrol];
    _bgScrol.contentSize = CGSizeMake(DEVICE_WIDTH*3, 0);
    _bgScrol.scrollEnabled = NO;
   
    [self createTableView];
    
    _selectBtn = [self.view viewWithTag:100];
    //网络请求
    [self getListData];
    
}
- (void)getListData{
    [Engine getGQinfoListWithCateA:nil CateB:nil CateC:nil Pcode:nil Ccode:nil Acode:nil Mlevel:nil Title:nil Pagesize:nil Pageindex:nil Group:nil Success:^(NSDictionary *dic) {
        NSArray *arr = [dic objectForKey:@"Item3"];
        if (arr.count>0) {
            NSMutableArray *dataArr = [GQinfoListModel mj_objectArrayWithKeyValuesArray:arr];
            if (!_objLeftArr) {
                _objLeftArr = [[NSMutableArray alloc]init];
            }
            _objLeftArr = dataArr;
        }
        [_tabView reloadData];
    } Error:^(NSError *error) {
        
    }];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}
- (void)navInfo{
    UIButton *searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 18, 18)];
    [searchBtn addTarget:self action:@selector(searchBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn setImage:[UIImage imageNamed:@"supply_searchBtn"] forState:UIControlStateNormal];
    //    [informationCardBtn sizeToFit];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:searchBtn];
    self.navigationItem.rightBarButtonItem = searchItem;
}
- (void)searchBtnClick:(UIButton *)btn{
    SupplySearchViewController *searchVC = [[SupplySearchViewController alloc]init];
    [self.navigationController pushViewController:searchVC animated:YES];
}



- (IBAction)btnClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    
    int tag = (int)btn.tag - 100;
    _selectBtn.selected = NO;
    btn.selected = YES;
    _selectBtn =  btn;
    //询价留言
    [UIView animateWithDuration:0.5 animations:^{
        _bgScrol.contentOffset = CGPointMake(DEVICE_WIDTH * tag, 0);
        
        float l = DEVICE_WIDTH/2;
        _redLine.center = CGPointMake(l/2+l*tag , _redLine.center.y);
    } completion:nil];
    
    
    
}





- (void)createTableView{
    
    
    float f=48+5;
    float height = DEVICE_HEIGHT-64-40-48-5;
    for (int i=0; i<2; i++) {
       
        UITableView *tabView = [[UITableView alloc]initWithFrame:CGRectMake(DEVICE_WIDTH*i, f, DEVICE_WIDTH, height) style:UITableViewStyleGrouped];
        [_bgScrol addSubview:tabView];
        tabView.backgroundColor =RGB(242, 244, 245, 1);
        tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tabView.delegate = self;
        tabView.dataSource = self;
        if (i==0) {
            _tabView = tabView;
        }
        
    }
    
    
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

#pragma mark - tabData
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _objLeftArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPHONE4||IS_IPHONE5) {
        return 75;
        
    }else{
        return 95;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MyCollectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell =[[[NSBundle mainBundle] loadNibNamed:@"MyCollectTableViewCell" owner:self options:nil]lastObject];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
        cell.flag = @"myLock_lock";
    }
    cell.model = _objLeftArr[indexPath.section];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SupplyInfoViewController *supplyInfo = [[SupplyInfoViewController alloc]init];
    [self.navigationController pushViewController:supplyInfo animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
