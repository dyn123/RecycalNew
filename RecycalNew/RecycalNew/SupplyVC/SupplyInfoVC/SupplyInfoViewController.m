//
//  SupplyInfoViewController.m
//  RecycalNew
//
//  Created by apple on 2017/8/5.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "SupplyInfoViewController.h"
#import "SupplyInfoHeadView.h"
#import "SupplyInfoWebView.h"
#import "SupplyInfoContactView.h"
#define HEAD_HEIGHT 475
#define WEB_HEIGHT 280
#define CONTACT_HEIGHT 220

@interface SupplyInfoViewController ()
@property (nonatomic,strong)UIScrollView *bgScrol;
@end

@implementation SupplyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title =  @"供求详情";
    _bgScrol = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT)];
    [self.view addSubview:_bgScrol];
    _bgScrol.contentSize = CGSizeMake(0, 1100);
    
    [self addHeadInfoView];
    [self addWebView];
    [self addContactView];
    //网络请求
    [Engine getDataDetailModelWithDid:nil DataType:nil Uid:nil Success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
    } Error:^(NSError *error) {
        
    }];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)addHeadInfoView{
    
    SupplyInfoHeadView *headView = [[[NSBundle mainBundle]loadNibNamed:@"SupplyInfoHeadView" owner:nil options:nil]lastObject];
    headView.frame = CGRectMake(0, 0, DEVICE_WIDTH, HEAD_HEIGHT);
    [_bgScrol addSubview:headView];
    
}
- (void)addWebView{
    SupplyInfoWebView *webView = [[[NSBundle mainBundle]loadNibNamed:@"SupplyInfoWebView" owner:nil options:nil]lastObject];
    webView.frame = CGRectMake(0, HEAD_HEIGHT, DEVICE_WIDTH, WEB_HEIGHT);
    [_bgScrol addSubview:webView];
}
- (void)addContactView{
    SupplyInfoContactView *contactView = [[[NSBundle mainBundle]loadNibNamed:@"SupplyInfoContactView" owner:nil options:nil]lastObject];
    contactView.frame = CGRectMake(0, HEAD_HEIGHT+WEB_HEIGHT, DEVICE_WIDTH, CONTACT_HEIGHT);
    [_bgScrol addSubview:contactView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
