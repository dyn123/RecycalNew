//
//  NSObject+viewHeightOffsetExtension.h
//  ACRepairNew
//
//  Created by administrator on 16/7/7.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (viewHeightOffsetExtension)
- (float) isOffsetFrameWithSubView:(UIView *)subView SuperView:(UIView *)superView keyBoard:(CGRect) keyBoardRect;
- (UIView *)findFirstResponser;
@end
