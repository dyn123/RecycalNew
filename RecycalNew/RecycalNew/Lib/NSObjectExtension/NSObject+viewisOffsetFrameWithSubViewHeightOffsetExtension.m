//
//  NSObject+viewHeightOffsetExtension.m
//  ACRepairNew
//
//  Created by administrator on 16/7/7.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "NSObject+viewHeightOffsetExtension.h"

@implementation NSObject (viewHeightOffsetExtension)
- (float) isOffsetFrameWithSubView:(UIView *)subView SuperView:(UIView *)superView keyBoard:(CGRect) keyBoardRect{
    float y = 0;
    float hits_des_h = 0;
    if([subView isKindOfClass:[UIView class]]&&[superView isKindOfClass:[UIView class]]){
        
        y = keyBoardRect.origin.y;
        CGRect his_des_to_self = [subView convertRect:subView.frame toView:superView];
        hits_des_h = (his_des_to_self.origin.y + his_des_to_self.size.height);
       
    }
     return y - hits_des_h;
}
- (UIView *)findFirstResponser{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    UIView *firstResponder = [keyWindow performSelector:@selector(firstResponder)];
    return firstResponder;
}

@end


