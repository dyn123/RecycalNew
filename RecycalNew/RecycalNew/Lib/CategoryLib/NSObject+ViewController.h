//
//  NSObject+ViewController.h
//  ACRepairNew
//
//  Created by mac on 16/6/29.
//  Copyright © 2016年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ViewController)

- (UIViewController *)currentViewController;
@end
