//
//  CALayer+LayerColor.h
//  PDOSystem
//
//  Created by apple on 2017/5/8.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (LayerColor)


- (void)setBorderColorFromUIColor:(UIColor *)color;

@end
