//
//  UIView+Extend.m
//  ACRepair
//
//  Created by mac on 16/5/25.
//  Copyright © 2016年 mac. All rights reserved.
//

#import "UIView+Extend.h"

@implementation UIView (Extend)
- (UIViewController *)viewController{
    for (UIView *next=[self superview]; next; next=next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}
@end
