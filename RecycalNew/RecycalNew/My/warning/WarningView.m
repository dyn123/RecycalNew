//
//  WarningView.m
//  RecycalNew
//
//  Created by apple on 2017/7/6.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "WarningView.h"
@interface WarningView()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *width;
@end
@implementation WarningView

- (void)awakeFromNib{
    [super awakeFromNib];
    
    [super awakeFromNib];
    if (IS_IPHONE4||IS_IPHONE5) {
        _width.constant = 250;
        
    }else{
        _width.constant = 310;
    }
    
}
- (IBAction)firstBtnClick:(id)sender {
    [self removeFromSuperview];
}

@end
