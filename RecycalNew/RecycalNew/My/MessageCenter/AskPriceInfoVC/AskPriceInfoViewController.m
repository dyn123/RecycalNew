//
//  AskPriceInfoViewController.m
//  RecycalNew
//
//  Created by apple on 2017/7/10.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "AskPriceInfoViewController.h"

#import "AskPriceInfoTableViewCell.h"

@interface AskPriceInfoViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation AskPriceInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"询价留言";
    self.tabBarController.tabBar.hidden = YES;
    
    [self createTable];
}

- (void)createTable{
    
    
    UITableView  *tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64+75, DEVICE_WIDTH, DEVICE_HEIGHT-64-70) style:UITableViewStyleGrouped];
    [self.view addSubview:tabView];
    tabView.backgroundColor =RGB(242, 244, 245, 1);
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tabView.delegate = self;
    tabView.dataSource = self;
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

#pragma mark - tabData
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 118;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    AskPriceInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell =[[[NSBundle mainBundle] loadNibNamed:@"AskPriceInfoTableViewCell" owner:self options:nil]lastObject];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
            
    }
        
    return cell;
        

    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 刷新
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    
}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"        ";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
