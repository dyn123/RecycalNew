//
//  SystemMsgTableViewCell.m
//  RecycalNew
//
//  Created by apple on 2017/7/10.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "SystemMsgTableViewCell.h"

@implementation SystemMsgTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)layoutSubviews{
    for (UIView *subView in self.subviews) {
        if([subView isKindOfClass:NSClassFromString(@"UITableViewCellDeleteConfirmationView")]) {
            
            // 拿到subView之后再获取子控件
            
            // 因为上面删除按钮是第二个加的所以下标是1
            UIView *deleteConfirmationView = subView.subviews[0];
            //改背景颜色
            deleteConfirmationView.backgroundColor = RGB(102, 102, 102, 1);
            UIImageView *deleteImage = [[UIImageView alloc] init];
            deleteImage.contentMode = UIViewContentModeScaleAspectFit;
            deleteImage.image = [UIImage imageNamed:@"msgCenter_askPrice_delete"];
            deleteImage.frame = CGRectMake(deleteConfirmationView.bounds.size.width/2-9, 20, 18, 18);
            [deleteConfirmationView addSubview:deleteImage];
            
            UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(0, deleteImage.frame.size.height+deleteImage.frame.origin.y+10, deleteConfirmationView.frame.size.width, 12)];
            lab.textAlignment = NSTextAlignmentCenter;
            lab.textColor = RGB(170, 170, 170, 1);
            lab.font = [UIFont systemFontOfSize:12];
            [deleteConfirmationView addSubview:lab];
            lab.text = @"删除";
            
            
            
        }
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
