//
//  MessageCenterViewController.m
//  RecycalNew
//
//  Created by apple on 2017/7/10.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "MessageCenterViewController.h"


#import "AskPriceTableViewCell.h"
#import "SystemMsgTableViewCell.h"


#import "AskPriceInfoViewController.h"
#import "SystemMsgInfoViewController.h"

@interface MessageCenterViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *leftView;

@property (weak, nonatomic) IBOutlet UIView *rightView;

@property (weak, nonatomic) IBOutlet UIView *redLine;

@property (nonatomic,strong)UITableView *leftTabView;
@property (nonatomic,strong)UITableView *rightTabView;
@property (nonatomic,strong)UIScrollView *bgScrol;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *point_trailing;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftPoint_trailing;



@property (nonatomic,strong)UIButton *selectBtn;

@end

@implementation MessageCenterViewController

- (void)awakeFromNib{
    [super awakeFromNib];
   
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (IS_IPHONE4||IS_IPHONE5) {
        _point_trailing.constant = 47;
        _leftPoint_trailing.constant = 47;
    }else{
        _point_trailing.constant = 58;
        _leftPoint_trailing.constant = 58;
    }
    
    self.navigationItem.title = @"消息中心";
    self.tabBarController.tabBar.hidden = YES;
    //添加UIScrollView
    _bgScrol = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64+40, DEVICE_WIDTH, DEVICE_HEIGHT)];
    [self.view addSubview:_bgScrol];
    _bgScrol.contentSize = CGSizeMake(DEVICE_WIDTH*2, 0);
  
    _bgScrol.scrollEnabled = NO;
   //添加table
    [self createLeftTableView:_bgScrol];
    [self createRightTableView:_bgScrol];
    
    _selectBtn = [self.view viewWithTag:100];
}

- (IBAction)priceBrnClick:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    //询价留言
    [UIView animateWithDuration:0.5 animations:^{
        _bgScrol.contentOffset = CGPointMake(0, 0);
        _redLine.center = CGPointMake(_leftView.center.x, _redLine.center.y);
    } completion:nil];
    
    _selectBtn.selected = NO;
    btn.selected = YES;
    _selectBtn = btn;
    
}
- (IBAction)systemBtnClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    _selectBtn.selected = NO;
    btn.selected = YES;
    _selectBtn = btn;
    //系统消息
    [UIView animateWithDuration:0.5 animations:^{
        
      _bgScrol.contentOffset = CGPointMake(DEVICE_WIDTH, 0);
        _redLine.center = CGPointMake(_rightView.center.x, _redLine.center.y);
    } completion:nil];
}


- (void)createLeftTableView:(UIScrollView *)scrolView{
    
    
    _leftTabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT-64-40) style:UITableViewStyleGrouped];
    [scrolView addSubview:_leftTabView];
    _leftTabView.backgroundColor =RGB(242, 244, 245, 1);
    _leftTabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _leftTabView.delegate = self;
    _leftTabView.dataSource = self;
    
}

- (void)createRightTableView:(UIScrollView *)scrolView{
    
    
    _rightTabView = [[UITableView alloc]initWithFrame:CGRectMake(DEVICE_WIDTH, 0, DEVICE_WIDTH, DEVICE_HEIGHT-64-40) style:UITableViewStyleGrouped];
    [scrolView addSubview:_rightTabView];
    _rightTabView.backgroundColor = [UIColor clearColor];
    _rightTabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _rightTabView.delegate = self;
    _rightTabView.dataSource = self;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

#pragma mark - tabData
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 73;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _leftTabView) {
        AskPriceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell =[[[NSBundle mainBundle] loadNibNamed:@"AskPriceTableViewCell" owner:self options:nil]lastObject];
            cell.selectionStyle =UITableViewCellSelectionStyleNone;
            
        }
        
        return cell;
        
    }else{
        
        SystemMsgTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell =[[[NSBundle mainBundle] loadNibNamed:@"SystemMsgTableViewCell" owner:self options:nil]lastObject];
            cell.selectionStyle =UITableViewCellSelectionStyleNone;
            
        }
        return cell;
    }
  
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _leftTabView) {
        
        AskPriceInfoViewController *askPrice = [[AskPriceInfoViewController alloc]init];
        [self.navigationController pushViewController:askPrice animated:YES];
    }else{
        
        SystemMsgInfoViewController *sysMsg = [[SystemMsgInfoViewController alloc]init];
        [self.navigationController pushViewController:sysMsg animated:YES];
    }

}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 刷新
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"        ";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
