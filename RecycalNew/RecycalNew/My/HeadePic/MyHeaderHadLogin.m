//
//  MyHeaderHadLogin.m
//  RecycalNew
//
//  Created by apple on 2017/7/6.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "MyHeaderHadLogin.h"

@interface MyHeaderHadLogin ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *headImg;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailing;

@end
@implementation MyHeaderHadLogin


- (void)awakeFromNib{
    [super awakeFromNib];
    _trailing.constant = -20;
}
#pragma mark - 上传头像

- (IBAction)headerPicBtnClick:(id)sender {
    UIImagePickerController *imgPicVC = [[UIImagePickerController alloc]init];
    imgPicVC.delegate = self;
    
    //创建提示框
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"选取图片" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *camer = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        imgPicVC.sourceType = UIImagePickerControllerSourceTypeCamera;
        [[self currentViewController] presentViewController:imgPicVC animated:YES completion:^{}];
    }];
    UIAlertAction *photo = [UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        imgPicVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [[self currentViewController] presentViewController:imgPicVC animated:YES completion:^{}];
    }];
    UIAlertAction *cacel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [alertC addAction:camer];
    }
    [alertC addAction:photo];
    [alertC addAction:cacel];
    [[self currentViewController] presentViewController:alertC animated:YES completion:nil];

}
#pragma mark - 相机的代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
    self.headImg.image = img;
    //把照片保存到相册
    SEL selectorToCall = @selector(image:didFinishSavingWithError:contextInfo:);
    UIImageWriteToSavedPhotosAlbum(img, self, selectorToCall, NULL);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if (error == nil){
        NSLog(@"Image was saved successfully.");
    } else {
        NSLog(@"An error happened while saving the image.");
        NSLog(@"Error = %@", error);
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [[self currentViewController] dismissViewControllerAnimated:YES completion:nil];
}

@end
