//
//  ModifyPsdViewController.m
//  RecycalNew
//
//  Created by apple on 2017/7/24.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "ModifyPsdViewController.h"


#import "ModifyPsdDefyViewController.h"
#import "ModifyPhoneDefyViewController.h"

@interface ModifyPsdViewController ()

@end

@implementation ModifyPsdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"设置密码";
}
#pragma mark - 密码验证
- (IBAction)psdModifyBtnClick:(id)sender {
    ModifyPsdDefyViewController *psdDefy = [[ModifyPsdDefyViewController alloc]init];
    [self.navigationController pushViewController:psdDefy animated:YES];
    
}
#pragma mark - 手机验证
- (IBAction)phoneModifyBtnClick:(id)sender {
    ModifyPhoneDefyViewController *phoneDefy = [[ModifyPhoneDefyViewController alloc]init];
    [self.navigationController pushViewController:phoneDefy animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
