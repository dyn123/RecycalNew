//
//  BangSettingViewController.m
//  RecycalNew
//
//  Created by apple on 2017/7/24.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "BangSettingViewController.h"

@interface BangSettingViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthItem;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightItem;

@end

@implementation BangSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"绑定设置";
    
    if (IS_IPHONE4||IS_IPHONE5) {
        
        _widthItem.constant = 122;
        _heightItem.constant = 92;
    }else if(IS_IPHONE6){
        _widthItem.constant = 122;
        _heightItem.constant = 92;
        
    }else if(IS_IPHONE6PLUS){
        
        _widthItem.constant = 132;
        _heightItem.constant = 100;
    }else{
        _widthItem.constant = 122;
        _heightItem.constant = 92;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
