//
//  SettingCenterViewController.m
//  RecycalNew
//
//  Created by apple on 2017/7/24.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "SettingCenterViewController.h"

#import "ModifyPsdViewController.h"
#import "ModifyPhoneViewController.h"

#import "BangSettingViewController.h"
#import "SettingSugestViewController.h"
@interface SettingCenterViewController ()

@end

@implementation SettingCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"设置";
    self.tabBarController.tabBar.hidden = YES;
}
- (IBAction)btnClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag-100) {
        case 0:{
            
            //修改密码
            ModifyPsdViewController *psdMody = [[ModifyPsdViewController alloc]init];
            [self.navigationController pushViewController:psdMody animated:YES];
        }
            
            break;
        case 1:{
            //修改手机
            ModifyPhoneViewController *phoneMody = [[ModifyPhoneViewController alloc]init];
            [self.navigationController pushViewController:phoneMody animated:YES];
        }
            
            break;
        case 2:{
            //绑定设置
            BangSettingViewController *bangSet = [[BangSettingViewController alloc]init];
            [self.navigationController pushViewController:bangSet animated:YES];
            
        }
            
            break;
        case 3:{
            
            //建议反馈
            SettingSugestViewController *sugest = [[SettingSugestViewController alloc]init];
            [self.navigationController pushViewController:sugest animated:YES];
        }
            
            break;
        case 4:{
            
        }
            
            break;
            
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
