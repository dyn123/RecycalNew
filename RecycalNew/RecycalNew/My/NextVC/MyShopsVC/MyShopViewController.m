//
//  MyShopViewController.m
//  RecycalNew
//
//  Created by apple on 2017/7/6.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "MyShopViewController.h"
#import "MyCollectTableViewCell.h"

#import "MyShopChooseView.h"
#import "MyShop.h"
#import "CustomBtnForTable.h"
@interface MyShopViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UIScrollView *bgScrol;
@property (weak, nonatomic) IBOutlet UIView *redLine;
@property (nonatomic,strong)UIButton *selectBtn;
@end

@implementation MyShopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
      self.navigationItem.title = @"我的商铺";
    self.tabBarController.tabBar.hidden = YES;
    
    _bgScrol = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64+40, DEVICE_WIDTH, DEVICE_HEIGHT)];
    [self.view addSubview:_bgScrol];
    _bgScrol.contentSize = CGSizeMake(DEVICE_WIDTH*3, 0);
    _bgScrol.scrollEnabled = NO;
    [self createShopView];
    [self createTableView];
    _selectBtn = [self.view viewWithTag:100];
    
}
- (IBAction)btnClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    
    int tag = (int)btn.tag - 100;
    _selectBtn.selected = NO;
    btn.selected = YES;
    _selectBtn =  btn;
    //询价留言
    [UIView animateWithDuration:0.5 animations:^{
        _bgScrol.contentOffset = CGPointMake(DEVICE_WIDTH * tag, 0);
        
        float l = DEVICE_WIDTH/2;
        _redLine.center = CGPointMake(l/2+l*tag , _redLine.center.y);
    } completion:nil];
    
   
    
}

- (void)createShopView{
    MyShop *shop = [[[NSBundle mainBundle]loadNibNamed:@"MyShop" owner:nil options:nil]lastObject];
    [_bgScrol addSubview:shop];
    shop.frame = CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT-64-40);
}
- (void)createTableView{
    
    
    float f=48+5;
    float height = DEVICE_HEIGHT-64-40-48-5;
    MyShopChooseView *cT = [[[NSBundle mainBundle]loadNibNamed:@"MyShopChooseView" owner:nil options:nil]lastObject];
    cT.frame = CGRectMake(DEVICE_WIDTH, 5, DEVICE_WIDTH, f);
    cT.dataArr = @[@"供应信息",@"求购信息"];
    [_bgScrol addSubview:cT];
    
    UITableView *tabView = [[UITableView alloc]initWithFrame:CGRectMake(DEVICE_WIDTH, f, DEVICE_WIDTH, height) style:UITableViewStyleGrouped];
    [_bgScrol addSubview:tabView];
    tabView.backgroundColor =RGB(242, 244, 245, 1);
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tabView.delegate = self;
    tabView.dataSource = self;
    
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

#pragma mark - tabData
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPHONE4||IS_IPHONE5) {
        return 75;
        
    }else{
        return 95;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MyCollectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell =[[[NSBundle mainBundle] loadNibNamed:@"MyCollectTableViewCell" owner:self options:nil]lastObject];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
        cell.flag = @"myLock_lock";
    }
    return cell;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
