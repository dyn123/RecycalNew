//
//  MyShopChooseView.m
//  RecycalNew
//
//  Created by apple on 2017/8/4.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "MyShopChooseView.h"
#import "CustomBtnForTable.h"
@interface MyShopChooseView()
@property (weak, nonatomic) IBOutlet CustomBtnForTable *chooseBtn;

@end
@implementation MyShopChooseView

- (void)setDataArr:(NSArray *)dataArr{
    
     [_chooseBtn.button setTitle:dataArr[0] forState:UIControlStateNormal];
    _chooseBtn.listArr = dataArr;
}

@end
