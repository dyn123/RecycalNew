//
//  MyScanViewController.m
//  RecycalNew
//
//  Created by apple on 2017/7/6.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "MyScanViewController.h"
#import "MyCollectTableViewCell.h"
#import "MyCollectNoPicTableViewCell.h"
#import "MyCollectNoPicPMTableViewCell.h"

#import "MyScanDeleteWarn.h"
@interface MyScanViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UIScrollView *bgScrol;
@property (weak, nonatomic) IBOutlet UIView *redLine;
@property (nonatomic,strong)UIButton *selectBtn;

@end

@implementation MyScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"浏览记录";
    self.tabBarController.tabBar.hidden = YES;
    [self createRightBtn];
    
    
    _bgScrol = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64+40, DEVICE_WIDTH, DEVICE_HEIGHT)];
    [self.view addSubview:_bgScrol];
    _bgScrol.contentSize = CGSizeMake(DEVICE_WIDTH*3, 0);
    _bgScrol.scrollEnabled = NO;
    [self createTableView];

    _selectBtn = [self.view viewWithTag:100];
}

- (void)createRightBtn{
    UIButton  *rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 70, 18)];
    // [rightBtn setBackgroundImage:[UIImage imageNamed:@"Goods_defy_btn"] forState:UIControlStateNormal];
    [rightBtn setTitle:@"清除全部" forState:UIControlStateNormal];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [rightBtn addTarget:self action:@selector(deleteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIBarButtonItem *righBar = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = righBar;
    
    
    
}

- (void)deleteBtnClick:(UIButton *)sender{
    
    
    MyScanDeleteWarn *warning = [[[NSBundle mainBundle]loadNibNamed:@"MyScanDeleteWarn" owner:nil options:nil]lastObject];
    warning.frame = CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT);
    UIWindow *window = [[[UIApplication sharedApplication]delegate]window];
    [window addSubview:warning];
}
- (IBAction)btnClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    
    int tag = (int)btn.tag - 100;
    
    _selectBtn.selected = NO;
    btn.selected = YES;
    _selectBtn = btn;
    
    //询价留言
    [UIView animateWithDuration:0.5 animations:^{
        _bgScrol.contentOffset = CGPointMake(DEVICE_WIDTH * tag, 0);
        
        float l = DEVICE_WIDTH/3;
        _redLine.center = CGPointMake(l/2+l*tag , _redLine.center.y);
    } completion:nil];
    
    
}


- (void)createTableView{
    
    for (int i=0; i<3; i++) {
        
        UITableView *tabView = [[UITableView alloc]initWithFrame:CGRectMake(DEVICE_WIDTH*i, 0, DEVICE_WIDTH, DEVICE_HEIGHT-64-40) style:UITableViewStyleGrouped];
        [_bgScrol addSubview:tabView];
        tabView.tag = 50+i;
        tabView.backgroundColor =RGB(242, 244, 245, 1);
        tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tabView.delegate = self;
        tabView.dataSource = self;
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

#pragma mark - tabData
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPHONE4||IS_IPHONE5) {
        if (tableView.tag==50) {
             return 75;
        }else{
             return 90;
        }
       
        
    }else{
        return 95;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag==50) {
        MyCollectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell =[[[NSBundle mainBundle] loadNibNamed:@"MyCollectTableViewCell" owner:self options:nil]lastObject];
            cell.selectionStyle =UITableViewCellSelectionStyleNone;
            cell.flag = @"msgCenter_askPrice_delete";
        }
        return cell;
    }else if (tableView.tag==51){
        MyCollectNoPicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell =[[[NSBundle mainBundle] loadNibNamed:@"MyCollectNoPicTableViewCell" owner:self options:nil]lastObject];
            cell.selectionStyle =UITableViewCellSelectionStyleNone;
            cell.flag = @"msgCenter_askPrice_delete";
        }
        return cell;
        
    }else{
        MyCollectNoPicPMTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell =[[[NSBundle mainBundle] loadNibNamed:@"MyCollectNoPicPMTableViewCell" owner:self options:nil]lastObject];
            cell.selectionStyle =UITableViewCellSelectionStyleNone;
            //cell.flag = @"msgCenter_askPrice_delete";
        }
        return cell;
        
    }
    
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 刷新
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"        ";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
