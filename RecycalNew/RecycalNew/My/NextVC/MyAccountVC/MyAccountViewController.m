//
//  MyAccountViewController.m
//  RecycalNew
//
//  Created by apple on 2017/7/6.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "MyAccountViewController.h"
#import "MyAccountSaveWarn.h"


#import "CompanyAuthViewController.h"
#import "NameAuthViewController.h"
@interface MyAccountViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *left;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *right;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneWidth;

@end

@implementation MyAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (IS_IPHONE4||IS_IPHONE5) {
         _left.constant = 30;
        _right.constant = 30;
        _phoneWidth.constant = 105;
        
    }else{
        _left.constant = 60;
        _right.constant = 60;
        _phoneWidth.constant = 124;
    }
    self.navigationItem.title = @"我的账户";
    self.tabBarController.tabBar.hidden = YES;
}
- (IBAction)btnClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag-100) {
        case 0:{
            
            //所属行业
            
        }
            break;
        case 1:{
            //所在地区
        }
            break;
        case 2:{
            //企业性质
        }
            break;
            
        default:
            break;
    }
}


#pragma mark - 实地认证
- (IBAction)companyAuthBtnClick:(id)sender {
    CompanyAuthViewController *company = [[CompanyAuthViewController alloc]init];
    [self.navigationController pushViewController:company animated:YES];
}
#pragma mark - 实名认证
- (IBAction)nameAuthBtnClick:(id)sender {
    NameAuthViewController *name = [[NameAuthViewController alloc]init];
    [self.navigationController pushViewController:name animated:YES];
}

#pragma mark - 保存按钮点击
- (IBAction)saveBtnClick:(id)sender {
    
    MyAccountSaveWarn *warn = [[[NSBundle mainBundle]loadNibNamed:@"MyAccountSaveWarn" owner:nil options:nil]lastObject];
    warn.frame = CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT);
    UIWindow *window = [[[UIApplication sharedApplication]delegate]window];
    [window addSubview:warn];
    
    warn.block = ^(){
        [self.navigationController popViewControllerAnimated:YES];
    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
