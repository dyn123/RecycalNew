//
//  MyAccountSaveWarn.h
//  RecycalNew
//
//  Created by apple on 2017/7/10.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^okBtnClickBlock)();

@interface MyAccountSaveWarn : UIView

@property (nonatomic,strong)okBtnClickBlock block;
@end
