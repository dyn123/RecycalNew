//
//  MyAccountSaveWarn.m
//  RecycalNew
//
//  Created by apple on 2017/7/10.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "MyAccountSaveWarn.h"


@interface MyAccountSaveWarn()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *width;

@end
@implementation MyAccountSaveWarn

- (IBAction)saveBtnClick:(id)sender {
    [self removeFromSuperview];
    self.block();
}

- (void)awakeFromNib{
    [super awakeFromNib];
    if (IS_IPHONE4||IS_IPHONE5) {
         _width.constant = 240;
        
    }else{
        _width.constant = 310;
    }
}

@end
