//
//  CompanyAuthViewController.m
//  RecycalNew
//
//  Created by apple on 2017/7/19.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "CompanyAuthViewController.h"

@interface CompanyAuthViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *img1;

@property (weak, nonatomic) IBOutlet UIImageView *img2;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthView;

@end

@implementation CompanyAuthViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"实地认证";
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    _widthView.constant = DEVICE_WIDTH *2;
}
#pragma mark - 调用相机
- (IBAction)addPicBtnClick:(id)sender {
    
    
    UIImagePickerController *imgPicVC = [[UIImagePickerController alloc]init];
    imgPicVC.delegate = self;
    
    //创建提示框
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"选取图片" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *camer = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        imgPicVC.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imgPicVC animated:YES completion:^{}];
    }];
    UIAlertAction *photo = [UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        imgPicVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imgPicVC animated:YES completion:^{}];
    }];
    UIAlertAction *cacel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [alertC addAction:camer];
    }
    [alertC addAction:photo];
    [alertC addAction:cacel];
    [self presentViewController:alertC animated:YES completion:nil];
    
}
#pragma mark - 相机的代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
    self.img1.image = img;
    //把照片保存到相册
    SEL selectorToCall = @selector(image:didFinishSavingWithError:contextInfo:);
    UIImageWriteToSavedPhotosAlbum(img, self, selectorToCall, NULL);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if (error == nil){
        NSLog(@"Image was saved successfully.");
    } else {
        NSLog(@"An error happened while saving the image.");
        NSLog(@"Error = %@", error);
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
