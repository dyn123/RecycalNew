//
//  MyLockViewController.m
//  RecycalNew
//
//  Created by apple on 2017/7/6.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "MyLockViewController.h"
#import "MyCollectTableViewCell.h"
#import "MyCollectNoPicTableViewCell.h"

@interface MyLockViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UIScrollView *bgScrol;
@property (weak, nonatomic) IBOutlet UIView *redLine;
@property (nonatomic,strong)UIButton *selectBtn;


@end

@implementation MyLockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"我的锁定";
    self.tabBarController.tabBar.hidden = YES;
    
    _bgScrol = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64+40, DEVICE_WIDTH, DEVICE_HEIGHT)];
    [self.view addSubview:_bgScrol];
    _bgScrol.contentSize = CGSizeMake(DEVICE_WIDTH*3, 0);
    _bgScrol.scrollEnabled = NO;
    [self createTableView];
    _selectBtn = [self.view viewWithTag:100];
}
- (IBAction)btnClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    
    int tag = (int)btn.tag - 100;
    
    _selectBtn.selected = NO;
    btn.selected = YES;
    _selectBtn = btn;
    
    
    //询价留言
    [UIView animateWithDuration:0.5 animations:^{
        _bgScrol.contentOffset = CGPointMake(DEVICE_WIDTH * tag, 0);
        
        float l = DEVICE_WIDTH/2;
        _redLine.center = CGPointMake(l/2+l*tag , _redLine.center.y);
    } completion:nil];
    
    
}


- (void)createTableView{
    
    for (int i=0; i<2; i++) {
        
        UITableView *tabView = [[UITableView alloc]initWithFrame:CGRectMake(DEVICE_WIDTH*i, 0, DEVICE_WIDTH, DEVICE_HEIGHT-64-40) style:UITableViewStyleGrouped];
        [_bgScrol addSubview:tabView];
        tabView.tag = 50+i;
        tabView.backgroundColor =RGB(242, 244, 245, 1);
        tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tabView.delegate = self;
        tabView.dataSource = self;
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

#pragma mark - tabData
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPHONE4||IS_IPHONE5) {
        if (tableView.tag==50) {
            return 75;
        }else{
            return 90;
        }
        
    }else{
        return 95;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag==50) {
        MyCollectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell =[[[NSBundle mainBundle] loadNibNamed:@"MyCollectTableViewCell" owner:self options:nil]lastObject];
            cell.selectionStyle =UITableViewCellSelectionStyleNone;
            cell.flag = @"myLock_lock";
        }
        return cell;
    }else{
        MyCollectNoPicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell =[[[NSBundle mainBundle] loadNibNamed:@"MyCollectNoPicTableViewCell" owner:self options:nil]lastObject];
            cell.selectionStyle =UITableViewCellSelectionStyleNone;
            cell.flag = @"myLock_lock";
        }
        return cell;
        
    }
    
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 刷新
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"        ";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
