//
//  MyCollectTableViewCell.h
//  RecycalNew
//
//  Created by apple on 2017/7/11.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GQinfoListModel.h"
@interface MyCollectTableViewCell : UITableViewCell
@property (nonatomic,strong)NSString *flag;
@property (nonatomic,strong)GQinfoListModel *model;

@end
