//
//  MyPublishViewController.m
//  RecycalNew
//
//  Created by apple on 2017/7/6.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "MyPublishViewController.h"
#import "MyPublishOKTableViewCell.h"
#import "MyPublishNoTableViewCell.h"
@interface MyPublishViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)UIScrollView *bgScrol;
@property (weak, nonatomic) IBOutlet UIView *redLine;
@property (nonatomic,strong)UIButton *selectBtn;
@end

@implementation MyPublishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"我的发布";
    self.tabBarController.tabBar.hidden = YES;
    
    _bgScrol = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64+40, DEVICE_WIDTH, DEVICE_HEIGHT)];
    [self.view addSubview:_bgScrol];
    _bgScrol.contentSize = CGSizeMake(DEVICE_WIDTH*3, 0);
    _bgScrol.scrollEnabled = NO;
    
     _selectBtn = [self.view viewWithTag:100];
    [self createTableView];
}
- (IBAction)btnClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    
    int tag = (int)btn.tag - 100;
    
    _selectBtn.selected = NO;
    btn.selected = YES;
    _selectBtn = btn;
    
    //询价留言
    [UIView animateWithDuration:0.5 animations:^{
        _bgScrol.contentOffset = CGPointMake(DEVICE_WIDTH * tag, 0);
        
        float l = DEVICE_WIDTH/3;
        _redLine.center = CGPointMake(l/2+l*tag , _redLine.center.y);
    } completion:nil];
    
}

- (void)createTableView{
    
    for (int i=0; i<3; i++) {
        
        UITableView *tabView = [[UITableView alloc]initWithFrame:CGRectMake(DEVICE_WIDTH*i, 0, DEVICE_WIDTH, DEVICE_HEIGHT-64-40) style:UITableViewStyleGrouped];
        [_bgScrol addSubview:tabView];
        tabView.tag = 50+i;
        tabView.backgroundColor =RGB(242, 244, 245, 1);
        tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tabView.delegate = self;
        tabView.dataSource = self;
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

#pragma mark - tabData
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 52){
        return 93;
    }else{
         return 73;
    }
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  
    if (tableView.tag == 52) {
        //审核未通过
        MyPublishNoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MyPublishNoTableViewCell" owner:nil options:nil]lastObject];
        }
        return cell;
        
    }else{
        //审核完成+审核中
        MyPublishOKTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MyPublishOKTableViewCell" owner:nil options:nil]lastObject];
        }
        if (tableView.tag == 51) {
            cell.imgName = @"myPublish_pointOrge";
        }
        return cell;
        
    }
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 刷新
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    
}


//设置tableview是否可编辑
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //这里是关键：这样写才能实现既能禁止滑动删除Cell，又允许在编辑状态下进行删除

    if (tableView.tag==51){
        //审核中 不编辑
        return UITableViewCellEditingStyleNone;
    }else {
        
        return UITableViewCellEditingStyleDelete;
    }
}
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 50){
        return @"                                               ";
    }else{
        return @"                    ";
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
