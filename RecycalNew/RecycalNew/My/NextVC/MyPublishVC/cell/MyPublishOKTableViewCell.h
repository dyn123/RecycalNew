//
//  MyPublishOKTableViewCell.h
//  RecycalNew
//
//  Created by apple on 2017/7/11.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPublishOKTableViewCell : UITableViewCell

@property (nonatomic,strong)NSString *imgName;

@property (nonatomic,assign)BOOL overdue;
@end
