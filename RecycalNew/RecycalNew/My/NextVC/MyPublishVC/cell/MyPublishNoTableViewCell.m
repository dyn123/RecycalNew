//
//  MyPublishNoTableViewCell.m
//  RecycalNew
//
//  Created by apple on 2017/7/11.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "MyPublishNoTableViewCell.h"
#import "MyPublishLeftRemoveNo.h"
@implementation MyPublishNoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)layoutSubviews{
    for (UIView *subView in self.subviews) {
        if([subView isKindOfClass:NSClassFromString(@"UITableViewCellDeleteConfirmationView")]) {
            
            // 拿到subView之后再获取子控件
            
            // 因为上面删除按钮是第二个加的所以下标是1
            UIView *deleteConfirmationView = subView.subviews[0];
            
            MyPublishLeftRemoveNo *remove = [[[NSBundle mainBundle]loadNibNamed:@"MyPublishLeftRemoveNo" owner:nil options:nil]lastObject];
            remove.frame = CGRectMake(0, 0, 125, 93);
            [deleteConfirmationView addSubview:remove];
            
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
