//
//  MyPublishOKTableViewCell.m
//  RecycalNew
//
//  Created by apple on 2017/7/11.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "MyPublishOKTableViewCell.h"

#import "MyPublishLeftRemove.h"
@interface MyPublishOKTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *point;
@property (weak, nonatomic) IBOutlet UIImageView *overdueImg;


@end
@implementation MyPublishOKTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setOverdue:(BOOL)overdue{
    if (overdue) {
        _overdueImg.hidden = NO;
    }else{
        _overdueImg.hidden = YES;
    }
}
- (void)layoutSubviews{
    for (UIView *subView in self.subviews) {
        if([subView isKindOfClass:NSClassFromString(@"UITableViewCellDeleteConfirmationView")]) {
            
            // 拿到subView之后再获取子控件
            
            // 因为上面删除按钮是第二个加的所以下标是1
            UIView *deleteConfirmationView = subView.subviews[0];
                        
            MyPublishLeftRemove *remove = [[[NSBundle mainBundle]loadNibNamed:@"MyPublishLeftRemove" owner:nil options:nil]lastObject];
            remove.frame = CGRectMake(0, 0, 250, 73);
            [deleteConfirmationView addSubview:remove];
            
        }
    }
}
- (void)setImgName:(NSString *)imgName{
    _point.image = [UIImage imageNamed:imgName];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
