//
//  MyViewController.m
//  RecycalNew
//
//  Created by apple on 2017/7/6.
//  Copyright © 2017年 apple. All rights reserved.
//
#import "MyViewController.h"
#import "MyHeaderHadLogin.h"
#import "MyHeaderNoLogin.h"


#import "WarningView.h"
#import "WarningVip.h"



#import "MessageCenterViewController.h"
#import "SettingCenterViewController.h"

#import "MyAccountViewController.h"
#import "MyShopViewController.h"
#import "MyVipViewController.h"
#import "MyPublishViewController.h"
#import "MyCollectViewController.h"
#import "HelpCenterViewController.h"
#import "MyLockViewController.h"
#import "MyPushViewController.h"
#import "MyScanViewController.h"
#import "HelpCenterViewController.h"


#define TABLE_HEADER_HEIGHT 44




@interface MyViewController ()
@property (weak, nonatomic) IBOutlet UIView *headVeiw;

@end



@implementation MyViewController

- (void)awakeFromNib{
    [super awakeFromNib];
 
  
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
   
    [self navView];
    
    MyHeaderHadLogin *hadLogin = [[[NSBundle mainBundle]loadNibNamed:@"MyHeaderHadLogin" owner:nil options:nil]firstObject];
    hadLogin.frame = CGRectMake(0, 0, DEVICE_WIDTH, 94);
    [_headVeiw addSubview:hadLogin];
  
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
}

- (IBAction)btnClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag-100) {
        case 0:{
            //我的账户
            MyAccountViewController *myAccount = [[MyAccountViewController alloc]init];
            [self.navigationController pushViewController:myAccount animated:YES];
        }
            
            break;
        case 1:{
            //我的商铺
            MyShopViewController *myShop = [[MyShopViewController alloc]init];
            [self.navigationController pushViewController:myShop animated:YES];
        }
            break;
        case 2:{
            //贵宾专区
            MyVipViewController *myVip = [[MyVipViewController alloc]init];
            [self.navigationController pushViewController:myVip animated:YES];
        }
            break;
        case 3:{
            //我的发布
            MyPublishViewController *myPublish = [[MyPublishViewController alloc]init];
            [self.navigationController pushViewController:myPublish animated:YES];
        }
            break;
        case 4:{
            //我的收藏
            MyCollectViewController *myCollect = [[MyCollectViewController alloc]init];
            [self.navigationController pushViewController:myCollect animated:YES];
        }
            break;
        case 5:{
            //我的锁定
            MyLockViewController *myLock = [[MyLockViewController alloc]init];
            [self.navigationController pushViewController:myLock animated:YES];
        }
            break;
        case 6:{
            //我的推送
            MyPushViewController *myPush = [[MyPushViewController alloc]init];
            [self.navigationController pushViewController:myPush animated:YES];
            
        }
            break;
        case 7:{
            //浏览记录
            MyScanViewController *myScan = [[MyScanViewController alloc]init];
            [self.navigationController pushViewController:myScan animated:YES];
        }
            break;
        case 8:{
            
            HelpCenterViewController *help = [[HelpCenterViewController alloc]init];
            [self.navigationController pushViewController:help animated:YES];
        }
            break;
       
            
        default:
            break;
            
    }
}


- (void)navView{
    self.navigationItem.title = @"我的";
    self.leftBtn.hidden = YES;
    
    
    UIButton *informationCardBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 18, 18)];
    [informationCardBtn addTarget:self action:@selector(messageBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [informationCardBtn setImage:[UIImage imageNamed:@"my_message"] forState:UIControlStateNormal];
//    [informationCardBtn sizeToFit];
    UIBarButtonItem *informationCardItem = [[UIBarButtonItem alloc] initWithCustomView:informationCardBtn];
    
    
    UIBarButtonItem *fixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpaceBarButtonItem.width = 10;
    
    
    UIButton *settingBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 18, 18)];
    [settingBtn addTarget:self action:@selector(settingBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [settingBtn setImage:[UIImage imageNamed:@"my_setting"] forState:UIControlStateNormal];
    [settingBtn sizeToFit];
    UIBarButtonItem *settingBtnItem = [[UIBarButtonItem alloc] initWithCustomView:settingBtn];
    
    
    
    self.navigationItem.rightBarButtonItems  = @[settingBtnItem,fixedSpaceBarButtonItem,informationCardItem];
    
}
#pragma mark - 消息按钮
- (void)messageBtnClick:(UIButton *)btn{
    [btn setImage:[UIImage imageNamed:@"my_message_on"] forState:UIControlStateNormal];
    MessageCenterViewController *msgCenter = [[MessageCenterViewController alloc]init];
    [self.navigationController pushViewController:msgCenter animated:YES];

    /*
    WarningVip *warning = [[[NSBundle mainBundle]loadNibNamed:@"WarningVip" owner:nil options:nil]lastObject];
    warning.frame = CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT);
    UIWindow *window = [[[UIApplication sharedApplication]delegate]window];
    [window addSubview:warning];
    */
    
}
#pragma mark - 设置按钮
- (void)settingBtnClick:(UIButton *)btn{
    
    SettingCenterViewController *setting = [[SettingCenterViewController alloc]init];
    [self.navigationController pushViewController:setting animated:YES];
    
    /*
     
    WarningView *warning = [[[NSBundle mainBundle]loadNibNamed:@"WarningView" owner:nil options:nil]lastObject];
    warning.frame = CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT);
    UIWindow *window = [[[UIApplication sharedApplication]delegate]window];
    [window addSubview:warning];
     
     */
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
