//
//  SearchViewController.m
//  RecycalNew
//
//  Created by apple on 2017/8/4.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchBarHeadView.h"



#import "SearchItemCollectionViewCell.h"
#import "SearchItemCollectionHeaderView.h"

#import "SearchListViewController.h"

#define cellId @"cellId"
#define headId @"headId"

#define CollectionItem_Height 48

@interface SearchViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>
{
    UICollectionView *_collectionView;
}



@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden = YES;
    
    
    
    
    [self searchBar];
    [self addCollectionView];
 
  
}

- (void)searchBar{
    
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 20, DEVICE_WIDTH, 44)];
    [self.view addSubview:v];
    SearchBarHeadView *searchView = [[[NSBundle mainBundle]loadNibNamed:@"SearchBarHeadView" owner:self options:nil]lastObject];
    searchView.titleArr = @[@"供应"];
    searchView.dataArr = @[@"供求",@"资产",@"拍卖",@"租赁"];
    searchView.frame = CGRectMake(0, 0, DEVICE_WIDTH, v.frame.size.height);
    [v addSubview:searchView];
   
}

- (void)addCollectionView{
    if (_collectionView==nil) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init]; // 自定义的布局对象
        
        flowLayout.minimumLineSpacing = 5;//行间距
        flowLayout.minimumInteritemSpacing = 5;//item间距
       
        flowLayout.headerReferenceSize=CGSizeMake(DEVICE_WIDTH, 45);//头视图的大小
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64, DEVICE_WIDTH,DEVICE_HEIGHT) collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.pagingEnabled = YES;
        
        _collectionView.showsVerticalScrollIndicator = YES;
        
        _collectionView.showsHorizontalScrollIndicator = NO;
        [self.view addSubview:_collectionView];
        
        // 注册cell、sectionHeader、sectionFooter
        //        [_collectionView registerClass:[VIPCollectionViewCell class] forCellWithReuseIdentifier:cellId];
        [_collectionView registerNib:[UINib nibWithNibName:@"SearchItemCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:cellId];
        
        [_collectionView registerNib:[UINib nibWithNibName:@"SearchItemCollectionHeaderView" bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader  withReuseIdentifier:headId];
    }
    
    
}
#pragma mark - section数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}
#pragma mark - 每个section中item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 4;
    
}


#pragma mark - 每个item的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((DEVICE_WIDTH-50)/3, CollectionItem_Height);
}
#pragma mark - 每个item的外边距  上 左  下  右
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 20, 0, 20);
}

#pragma mark - 每个CollectionView展示的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //在这里注册自定义的XIBcell否则会提示找不到标示符指定的cell
    //    UINib *nib = [UINib nibWithNibName:@"VIPCollectionViewCell"bundle: [NSBundle mainBundle]];
    //    [collectionView registerNib:nib forCellWithReuseIdentifier:cellId];
    
    
    SearchItemCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    
    return cell;
    
}
#pragma mark - header视图
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        SearchItemCollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headId forIndexPath:indexPath];
       
        if (indexPath.section==0) {
            headerView.title.text = @"历史记录";
            
        }else{
            headerView.title.text = @"热门搜索";
            
        }
        
        reusableview = headerView;
    }
   
    
    
    return reusableview;
}
#pragma mark - 点击CollectionViewItem
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    SearchListViewController *searchList = [[SearchListViewController alloc]init];
    [self.navigationController pushViewController:searchList animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
