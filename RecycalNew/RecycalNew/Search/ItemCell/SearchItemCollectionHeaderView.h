//
//  SearchItemCollectionHeaderView.h
//  RecycalNew
//
//  Created by apple on 2017/8/4.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchItemCollectionHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
