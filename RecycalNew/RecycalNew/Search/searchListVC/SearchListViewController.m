//
//  SearchListViewController.m
//  RecycalNew
//
//  Created by apple on 2017/8/4.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "SearchListViewController.h"

#import "SearchListNoResultView.h"



#import "SearchBarHeadView.h"
#import "MyCollectTableViewCell.h" //供求


@interface SearchListViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation SearchListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden = YES;
    
    
    
    
    [self searchBar];
   // [self addTableView];
    //没有搜到结果
    [self addNoResultView];
    
}

- (void)addNoResultView{
    
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 64, DEVICE_WIDTH, DEVICE_HEIGHT-64)];
    [self.view addSubview:v];
    
    SearchListNoResultView *noResult = [[[NSBundle mainBundle]loadNibNamed:@"SearchListNoResultView" owner:nil options:nil]lastObject];
    noResult.frame = CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT-64);
    [v addSubview:noResult];
}
- (void)addTableView{
    UITableView *tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, DEVICE_WIDTH, DEVICE_HEIGHT-64-35) style:UITableViewStyleGrouped];
    [self.view addSubview:tabView];
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tabView.delegate = self;
    tabView.dataSource = self;
}


#pragma mark - tabViewData
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return  5;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyCollectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell =[[[NSBundle mainBundle] loadNibNamed:@"MyCollectTableViewCell" owner:self options:nil]lastObject];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (void)searchBar{
    
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 20, DEVICE_WIDTH, 44)];
    [self.view addSubview:v];
    
    
    SearchBarHeadView *searchView = [[[NSBundle mainBundle]loadNibNamed:@"SearchBarHeadView" owner:self options:nil]lastObject];
    searchView.titleArr = @[@"供应"];
    searchView.dataArr = @[@"供求",@"资产",@"拍卖",@"租赁"];
    searchView.frame = CGRectMake(0, 0, DEVICE_WIDTH, v.frame.size.height);
    [v addSubview:searchView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
