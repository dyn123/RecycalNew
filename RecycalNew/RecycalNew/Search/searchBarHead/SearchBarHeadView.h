//
//  SearchBarHeadView.h
//  RecycalNew
//
//  Created by apple on 2017/8/4.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBtnForTable.h"
@interface SearchBarHeadView : UIView

@property (weak, nonatomic) IBOutlet UIView *searchBar;

@property (nonatomic,strong)NSArray *titleArr;
@property (nonatomic,strong)NSArray *dataArr;

@property (nonatomic,assign)id delegate;
@end
@protocol SearchBarHeadViewDelegate <NSObject>



@end
