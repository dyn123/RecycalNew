//
//  SearchBarHeadView.m
//  RecycalNew
//
//  Created by apple on 2017/8/4.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "SearchBarHeadView.h"
#import "CustomSearchBar.h"

#import "SearchListViewController.h"

@interface SearchBarHeadView()<UISearchBarDelegate>
@end
@implementation SearchBarHeadView


- (void)layoutSubviews{
    [super layoutSubviews];
    CustomSearchBar *search  = [[CustomSearchBar alloc]initWithFrame:CGRectMake(0, 0, self.searchBar.frame.size.width, self.searchBar.frame.size.height)];

    [self.searchBar addSubview:search];
    
}
- (IBAction)backBtnClick:(id)sender {
    UIViewController *vc =  self.viewController;
    
    [vc.navigationController popViewControllerAnimated:YES];
    
//    [self.viewController.navigationController popViewControllerAnimated:YES];
}
- (IBAction)chooseBtnClick:(id)sender {
    
    
}

#pragma mark - 搜索按钮
- (IBAction)searchBtnClick:(id)sender {
    SearchListViewController *searchListVC = [[SearchListViewController alloc]init];
    [self.viewController.navigationController pushViewController:searchListVC animated:YES];
}









@end
