//
//  RentListViewController.m
//  RecycalNew
//
//  Created by apple on 2017/8/4.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "RentListViewController.h"
#import "SupplySearchViewController.h"
#import "RentInfoViewController.h"

#import "RentListTableViewCell.h"
#import "ChooseHeadTitle.h"

#import "RentInfoListModel.h"
#define HEADTITLE_HEIGHT 48

@interface RentListViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)UITableView *tabView;
@property (nonatomic,copy)NSMutableArray *objectArray;
@end

@implementation RentListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _objectArray = [NSMutableArray array];
    self.navigationItem.title = @"租赁信息";
    self.tabBarController.tabBar.hidden = YES;
    [self navInfo];

    [self createTableView];
    //网络请求
    [Engine getLeaseDataInfoListWithCateA:nil CateB:nil CateC:nil Pcode:nil Ccode:nil Acode:nil InfoType:nil Sesrch:nil Pagesize:nil Pageindex:nil Success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        NSMutableArray *arr = [dic objectForKey:@"Item3"];
        _objectArray = [RentInfoListModel mj_keyValuesArrayWithObjectArray:arr];
        
        [_tabView reloadData];
    } Error:^(NSError *error) {
        
    }];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
}

- (void)createTableView{
    
    
    float f=112;
    float height = DEVICE_HEIGHT-64-40-48-5;

        
    UITableView *tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, f, DEVICE_WIDTH, height) style:UITableViewStyleGrouped];

    tabView.backgroundColor =RGB(242, 244, 245, 1);
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tabView.delegate = self;
    tabView.dataSource = self;
 
    [self.view addSubview:tabView];
    _tabView = tabView;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

#pragma mark - tabData
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _objectArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPHONE4||IS_IPHONE5) {
        return 75;
        
    }else{
        return 90;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RentListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell =[[[NSBundle mainBundle] loadNibNamed:@"RentListTableViewCell" owner:self options:nil]lastObject];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RentInfoViewController *rentInfo = [[RentInfoViewController alloc]init];
    [self.navigationController pushViewController:rentInfo animated:YES];
}

- (void)downChooseView{
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0,64, DEVICE_WIDTH, 48)];
    [self.view addSubview:v];
    
    NSArray *title = @[@"所在仓惨",@"所属分类",@"入仓时间"];
    NSArray *dataArr = @[@[@"仓库1",@"仓库2",@"仓库3"],@[@"二手设备",@"回收机床",@"废铁"],@[@"一个月",@"两个月",@"一年"]];
    ChooseHeadTitle *headTitle = [[[NSBundle mainBundle]loadNibNamed:@"ChooseHeadTitle" owner:nil options:nil]lastObject];
    headTitle.frame = CGRectMake(0, 0, DEVICE_WIDTH, HEADTITLE_HEIGHT);
    [v addSubview:headTitle];
    headTitle.titleArr = title;
    headTitle.dataArr = dataArr;
    
    
}
- (void)navInfo{
    UIButton *searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 18, 18)];
    [searchBtn addTarget:self action:@selector(searchBtnClik:) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn setImage:[UIImage imageNamed:@"supply_searchBtn"] forState:UIControlStateNormal];
    //    [informationCardBtn sizeToFit];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:searchBtn];
    self.navigationItem.rightBarButtonItem = searchItem;
}
- (void)searchBtnClik:(UIButton *)btn{
    SupplySearchViewController *searchVC = [[SupplySearchViewController alloc]init];
    [self.navigationController pushViewController:searchVC animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
