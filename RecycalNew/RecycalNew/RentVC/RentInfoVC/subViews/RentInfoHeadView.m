//
//  RentInfoHeadView.m
//  RecycalNew
//
//  Created by apple on 2017/8/10.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "RentInfoHeadView.h"

@interface RentInfoHeadView()<UIScrollViewDelegate>
{
    UIPageControl *_pageCtrl;
}
@property (weak, nonatomic) IBOutlet UIScrollView *headerScroll;

@end
@implementation RentInfoHeadView

- (void)layoutSubviews{
    [super layoutSubviews];
    
    
    _headerScroll.delegate = self;
    _headerScroll.contentSize = CGSizeMake(DEVICE_WIDTH*2, 0);
    _headerScroll.pagingEnabled = YES;
    _headerScroll.bounces = YES;
    _headerScroll.showsHorizontalScrollIndicator = NO;
    _headerScroll.showsVerticalScrollIndicator = NO;
    
    
    for (int i =0; i < 2; i++) {
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(i*DEVICE_WIDTH, 0, DEVICE_WIDTH, _headerScroll.frame.size.height)];
        [_headerScroll addSubview:imgView];
        imgView.image = [UIImage imageNamed:@"supplyInfo_head"];
    }
    
    
    
    //pageController
    
    _pageCtrl = [[UIPageControl alloc]initWithFrame:CGRectMake(0, _headerScroll.bounds.size.height-20, DEVICE_WIDTH, 20)];
    [self addSubview:_pageCtrl];
    
    _pageCtrl.currentPage     = 0;
    _pageCtrl.numberOfPages   = 2;
    _pageCtrl.pageIndicatorTintColor = [UIColor whiteColor];
    _pageCtrl.currentPageIndicatorTintColor = RGB(231, 0, 18, 1);
    [_pageCtrl addTarget:self action:@selector(pageTurn:) forControlEvents:UIControlEventValueChanged];
    
    
}

#pragma mark - scroll&page
- (void)pageTurn:(UIPageControl *)sender{
    
    [_headerScroll setContentOffset:CGPointMake(sender.currentPage * DEVICE_WIDTH, 0) animated:YES];
    
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    _pageCtrl.currentPage = scrollView.contentOffset.x/DEVICE_WIDTH;
}


@end
