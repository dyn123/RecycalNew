//
//  RentInfoContactView.m
//  RecycalNew
//
//  Created by apple on 2017/8/10.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "RentInfoContactView.h"

#import "RentLeaveMsgViewController.h"
@implementation RentInfoContactView

- (IBAction)leaveMsgBtnClick:(id)sender {
    [self.viewController.navigationController pushViewController:[RentLeaveMsgViewController new] animated:YES];
}

@end
