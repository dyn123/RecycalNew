//
//  RentInfoViewController.m
//  RecycalNew
//
//  Created by apple on 2017/8/10.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "RentInfoViewController.h"


#import "RentInfoHeadView.h"
#import "RentInfoContentView.h"
#import "RentInfoContactView.h"


#define HEAD_HEIGHT 522

#define CONTENT_HEIGHT 300
#define CONTACT_HEIGHT 266
@interface RentInfoViewController ()
@property (nonatomic,strong)UIScrollView *bgScrol;
@end

@implementation RentInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"租赁信息";
    _bgScrol = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT)];
    [self.view addSubview:_bgScrol];
    _bgScrol.contentSize = CGSizeMake(0, 1100);
    [self addHeadInfoView];
   
    [self addContentView];
    [self addContactView];
}

- (void)addHeadInfoView{
    
    RentInfoHeadView *headView = [[[NSBundle mainBundle]loadNibNamed:@"RentInfoHeadView" owner:nil options:nil]lastObject];
    headView.frame = CGRectMake(0, 0, DEVICE_WIDTH, HEAD_HEIGHT);
    [_bgScrol addSubview:headView];
    
}



- (void)addContentView{
    RentInfoContentView *webView = [[[NSBundle mainBundle]loadNibNamed:@"RentInfoContentView" owner:nil options:nil]lastObject];
    webView.frame = CGRectMake(0,HEAD_HEIGHT, DEVICE_WIDTH, CONTENT_HEIGHT);
    [_bgScrol addSubview:webView];
}

- (void)addContactView{
    RentInfoContactView *contactView = [[[NSBundle mainBundle]loadNibNamed:@"RentInfoContactView" owner:nil options:nil]lastObject];
    contactView.frame = CGRectMake(0, HEAD_HEIGHT+CONTENT_HEIGHT, DEVICE_WIDTH, CONTACT_HEIGHT);
    [_bgScrol addSubview:contactView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
