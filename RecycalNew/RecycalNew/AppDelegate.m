//
//  AppDelegate.m
//  RecycalNew
//
//  Created by apple on 2017/7/6.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "OrderViewController.h"
#import "PublishViewController.h"
#import "FoundViewController.h"
#import "MyViewController.h"


#import "LoginViewController.h"
#import "BaseTabBarViewController.h"




@interface AppDelegate ()
{
    BaseTabBarViewController *tabBarC;

}
@end

@implementation AppDelegate




- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    NSLog(@"沙盒路径:%@", NSHomeDirectory());
    
//    
//    [[ReadAndWriteDateTools tools] readCateWithFirstCate:model.D_CidA SecondCate:nil ThirCate:nil plistName:@"6"][0];
//    
    
    
    
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    [self.window makeKeyAndVisible];
    self.window.backgroundColor = [UIColor whiteColor];
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    
//注释掉今天，才测试登录
    [self tabBarViewController];
    
    
    
    
//    NSInteger count =(NSInteger) [[NSUserDefaults standardUserDefaults]objectForKey:@"runTime"];
//    if (!count) {
        //获取省份信息
        /*注释掉今天，才测试登录
         [[ReadAndWriteDateTools tools]writeAllArea];
        
        [[ReadAndWriteDateTools tools] writeAllCategory];
        */
//        [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"runTime"];
//        [[NSUserDefaults standardUserDefaults]synchronize];
//    }
   
    
    return YES;
}

- (void)tabBarViewController{
    
    UIImage * mainImg = [[UIImage imageNamed:@"tabBar_main"]
                         imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage * mainImgH=[[UIImage imageNamed:@"tabBar_main_high"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UINavigationController *mainNav = [[UINavigationController alloc]initWithRootViewController:[MainViewController new]];
    mainNav.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"首页" image:mainImg selectedImage:mainImgH];
    
    
    UIImage * orderImg = [[UIImage imageNamed:@"tabBar_order"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage * orderImgH=[[UIImage imageNamed:@"tabBar_order_high"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UINavigationController *orderNav = [[UINavigationController alloc]initWithRootViewController:[OrderViewController new]];
    orderNav.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"订阅" image:orderImg selectedImage:orderImgH];
    
    
    UIImage * publishImg = [[UIImage imageNamed:@"tabBar_publish"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage * publishImgH=[[UIImage imageNamed:@"tabBar_publish_high"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UINavigationController *publishNav = [[UINavigationController alloc]initWithRootViewController:[PublishViewController new]];
    publishNav.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"发布" image:publishImg selectedImage:publishImgH];
    
    UIImage * foundImg = [[UIImage imageNamed:@"tabBar_found"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage * foundImgH=[[UIImage imageNamed:@"tabBar_found_high"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UINavigationController *foundNav = [[UINavigationController alloc]initWithRootViewController:[FoundViewController new]];
    foundNav.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"发现" image:foundImg selectedImage:foundImgH];
    
    UIImage * myImg = [[UIImage imageNamed:@"tabBar_my"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage * myImgH=[[UIImage imageNamed:@"tabBar_my_high"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UINavigationController *myNav = [[UINavigationController alloc]initWithRootViewController:[MyViewController new]];
    myNav.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"我的" image:myImg selectedImage:myImgH];
    
    tabBarC = [[BaseTabBarViewController alloc]init];
    tabBarC.viewControllers = @[mainNav,orderNav,publishNav,foundNav,myNav];
    tabBarC.tabBar.tintColor = [UIColor redColor];
    
    UIWindow *window =  [[UIApplication sharedApplication]delegate].window;
    window.rootViewController = tabBarC;
   
//    window.rootViewController = [[UINavigationController alloc]initWithRootViewController:[[LoginViewController alloc]init]];
  
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;



- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"RecycalNew"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                    
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}





@end
