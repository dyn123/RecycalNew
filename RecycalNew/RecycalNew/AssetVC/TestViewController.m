//
//  TestViewController.m
//  RecycalNew
//
//  Created by apple on 2017/8/9.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "TestViewController.h"
#import "SearchBarHeadView.h"
@interface TestViewController ()

@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//     self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBar.hidden = YES;
    [self searchBar];
}
- (void)searchBar{
    
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 20, DEVICE_WIDTH, 44)];
    [self.view addSubview:v];
    SearchBarHeadView *searchView = [[[NSBundle mainBundle]loadNibNamed:@"SearchBarHeadView" owner:self options:nil]lastObject];
    searchView.titleArr = @[@"供应"];
    searchView.dataArr = @[@"供求",@"资产",@"拍卖",@"租赁"];
    searchView.frame = CGRectMake(0, 0, DEVICE_WIDTH, v.frame.size.height);
    [v addSubview:searchView];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
