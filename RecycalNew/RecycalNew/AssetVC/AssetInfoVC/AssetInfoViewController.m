//
//  AssetInfoViewController.m
//  RecycalNew
//
//  Created by apple on 2017/8/7.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "AssetInfoViewController.h"

#import "AssetInfoHeadView.h"
#import "AssetInfoContentView.h"
#import "AssetInfoContentOtherView.h"
#import "AssetInfoContactView.h"

#define HEAD_HEIGHT 485
#define CONTENT_HEIGHT  489
#define CONTACT_HEIGHT 159

@interface AssetInfoViewController ()

@property (nonatomic,strong)UIScrollView *bgScrol;

@end

@implementation AssetInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title =  @"资产详情";
    _bgScrol = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT)];
    [self.view addSubview:_bgScrol];
    _bgScrol.contentSize = CGSizeMake(0, 1260);
    
    [self addHeadInfoView];
    [self addContentView];
    [self addContactView];
}
- (void)addHeadInfoView{
    
    AssetInfoHeadView *headView = [[[NSBundle mainBundle]loadNibNamed:@"AssetInfoHeadView" owner:nil options:nil]lastObject];
    headView.frame = CGRectMake(0, 0, DEVICE_WIDTH, HEAD_HEIGHT);
    [_bgScrol addSubview:headView];
    
}
- (void)addContentView{
    AssetInfoContentView *contentView = [[[NSBundle mainBundle]loadNibNamed:@"AssetInfoContentView" owner:nil options:nil]lastObject];
    contentView.frame = CGRectMake(0, HEAD_HEIGHT, DEVICE_WIDTH, 244);
    [_bgScrol addSubview:contentView];
    
    
    AssetInfoContentOtherView *contentOther = [[[NSBundle mainBundle]loadNibNamed:@"AssetInfoContentOtherView" owner:nil options:nil]lastObject];
    contentOther.frame = CGRectMake(0, HEAD_HEIGHT+244, DEVICE_WIDTH, CONTENT_HEIGHT-244);
    [_bgScrol addSubview:contentOther];
}
- (void)addContactView{
    AssetInfoContactView *contactView = [[[NSBundle mainBundle]loadNibNamed:@"AssetInfoContactView" owner:nil options:nil]lastObject];
    contactView.frame = CGRectMake(0, HEAD_HEIGHT+CONTENT_HEIGHT, DEVICE_WIDTH, CONTACT_HEIGHT);
    [_bgScrol addSubview:contactView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
