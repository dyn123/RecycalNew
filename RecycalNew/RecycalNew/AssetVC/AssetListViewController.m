//
//  AssetListViewController.m
//  RecycalNew
//
//  Created by apple on 2017/8/4.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "AssetListViewController.h"
#import "AssetInfoViewController.h"
#import "SupplySearchViewController.h"
#import "MyCollectNoPicTableViewCell.h"


#import "ChooseHeadTitle.h"
#define HEADTITLE_HEIGHT 48
@interface AssetListViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *redLine;
@property (nonatomic,strong)UIButton *selectBtn;
@property (nonatomic,strong)UIScrollView *bgScrol;

@end

@implementation AssetListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"资产处置";
    self.tabBarController.tabBar.hidden = YES;
    [self navInfo];
    
    _bgScrol = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64+40, DEVICE_WIDTH, DEVICE_HEIGHT)];
    [self.view addSubview:_bgScrol];
    _bgScrol.contentSize = CGSizeMake(DEVICE_WIDTH*3, 0);
    _bgScrol.scrollEnabled = NO;
    
//    [self downChooseView];
    [self createTableView];
    _selectBtn = [self.view viewWithTag:100];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
}
- (void)navInfo{
    UIButton *searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 18, 18)];
    [searchBtn addTarget:self action:@selector(searchBtnClik:) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn setImage:[UIImage imageNamed:@"supply_searchBtn"] forState:UIControlStateNormal];
    //    [informationCardBtn sizeToFit];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:searchBtn];
    self.navigationItem.rightBarButtonItem = searchItem;
}
- (void)searchBtnClik:(UIButton *)btn{
    SupplySearchViewController *searchVC = [[SupplySearchViewController alloc]init];
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (void)downChooseView{
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 109, DEVICE_WIDTH, 48)];
    [self.view addSubview:v];
    
    NSArray *title = @[@"所在仓惨",@"所属分类",@"入仓时间"];
    NSArray *dataArr = @[@[@"仓库1",@"仓库2",@"仓库3"],@[@"二手设备",@"回收机床",@"废铁"],@[@"一个月",@"两个月",@"一年"]];
    ChooseHeadTitle *headTitle = [[[NSBundle mainBundle]loadNibNamed:@"ChooseHeadTitle" owner:nil options:nil]lastObject];
    headTitle.frame = CGRectMake(0, 0, DEVICE_WIDTH, HEADTITLE_HEIGHT);
    [v addSubview:headTitle];
    headTitle.titleArr = title;
    headTitle.dataArr = dataArr;
    
    
}


- (IBAction)btnClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    
    int tag = (int)btn.tag - 100;
    _selectBtn.selected = NO;
    btn.selected = YES;
    _selectBtn =  btn;
    //询价留言
    [UIView animateWithDuration:0.5 animations:^{
        _bgScrol.contentOffset = CGPointMake(DEVICE_WIDTH * tag, 0);
        
        float l = DEVICE_WIDTH/2;
        _redLine.center = CGPointMake(l/2+l*tag , _redLine.center.y);
    } completion:nil];
    
    
    
}





- (void)createTableView{
    
    
    float f=48+5;
    float height = DEVICE_HEIGHT-64-40-48-5;
    for (int i=0; i<2; i++) {
        
        UITableView *tabView = [[UITableView alloc]initWithFrame:CGRectMake(DEVICE_WIDTH*i, f, DEVICE_WIDTH, height) style:UITableViewStyleGrouped];
        [_bgScrol addSubview:tabView];
        tabView.backgroundColor =RGB(242, 244, 245, 1);
        tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tabView.delegate = self;
        tabView.dataSource = self;
    }
    
    
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

#pragma mark - tabData
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPHONE4||IS_IPHONE5) {
        return 75;
        
    }else{
        return 95;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MyCollectNoPicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell =[[[NSBundle mainBundle] loadNibNamed:@"MyCollectNoPicTableViewCell" owner:self options:nil]lastObject];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
        cell.flag = @"myLock_lock";
    }
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AssetInfoViewController *supplyInfo = [[AssetInfoViewController alloc]init];
    [self.navigationController pushViewController:supplyInfo animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
