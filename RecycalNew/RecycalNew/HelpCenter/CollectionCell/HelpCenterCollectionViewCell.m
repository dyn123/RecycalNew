//
//  HelpCenterCollectionViewCell.m
//  RecycalNew
//
//  Created by apple on 2017/6/30.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "HelpCenterCollectionViewCell.h"
@interface HelpCenterCollectionViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
@implementation HelpCenterCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor whiteColor];
}
- (void)setImgName:(NSString *)imgName{
    _img.image = [UIImage imageNamed:imgName];
}
- (void)setTitleName:(NSString *)titleName{
    _title.text = titleName;
}
@end
