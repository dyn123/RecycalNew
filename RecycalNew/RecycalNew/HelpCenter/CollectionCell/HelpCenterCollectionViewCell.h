//
//  HelpCenterCollectionViewCell.h
//  RecycalNew
//
//  Created by apple on 2017/6/30.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpCenterCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong)NSString *imgName;
@property (nonatomic,strong)NSString *titleName;
@end
