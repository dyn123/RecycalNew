//
//  ContactServiceViewController.m
//  RecycalNew
//
//  Created by apple on 2017/6/30.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "ContactServiceViewController.h"


#import "PhoneServiceViewController.h"
#import "SuggestViewController.h"
@interface ContactServiceViewController ()

@end

@implementation ContactServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationItem setTitle:@"联系客服"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//电话预约
- (IBAction)PhoneServiceBtnClick:(id)sender {
    PhoneServiceViewController *phoneService = [[PhoneServiceViewController alloc]init];
    [self.navigationController pushViewController:phoneService animated:YES];
}

//在线客服

//建议问题
- (IBAction)SuggestServiceBtnClick:(id)sender {
    SuggestViewController *suggest = [[SuggestViewController alloc]init];
    [self.navigationController pushViewController:suggest animated:YES];
}

@end
