//
//  HelpCenterViewController.m
//  RecycalNew
//
//  Created by apple on 2017/6/30.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "HelpCenterViewController.h"
#import "HelpCenterCollectionViewCell.h"

#import "WebPreViewController.h"
#import "ContactServiceViewController.h"
#import "ProductCenterViewController.h"
#import "SomeQuesViewController.h"
@interface HelpCenterViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>
{
    UICollectionView *_collectionView;
    NSArray *imgArr;
    NSArray *titleArr;
}
#define cellId @"cellId"
#define CollectionItem_Height 96

@end

@implementation HelpCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.navigationController.navigationBarHidden = YES;
    [self.navigationItem setTitle:@"帮助中心"];
    self.tabBarController.tabBar.hidden = YES;
    imgArr = @[@"HelpCenter_Img1",@"HelpCenter_Img2",@"HelpCenter_Img3",@"HelpCenter_Img4"];
    titleArr = @[@"网站概况",@"产品中心",@"常见问题",@"系统留言"];
    [self addCollectionView];
}
- (void)addCollectionView{
    if (_collectionView==nil) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init]; // 自定义的布局对象
        
        flowLayout.minimumLineSpacing = 5;//每个collectionView横向间距(最小)
        flowLayout.minimumInteritemSpacing = 0;//每个collectionView纵向间距（最小）
        
        
        flowLayout.sectionInset = UIEdgeInsetsMake(5, 0, 0, 5);//上左下右
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH,DEVICE_HEIGHT) collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = BACKGROUND_COLOR;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [self.view addSubview:_collectionView];
        
        // 注册cell、sectionHeader、sectionFooter
        //        [_collectionView registerClass:[VIPCollectionViewCell class] forCellWithReuseIdentifier:cellId];
        [_collectionView registerNib:[UINib nibWithNibName:@"HelpCenterCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:cellId];
    }
    
    
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    ;
    //    return arr.count*2;
    
    return 4;
    
}


#pragma mark - 每个item的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((DEVICE_WIDTH-20)/3, CollectionItem_Height);
}
#pragma mark - 每个item的外边距  上 左  下  右
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 0, 0, 5);
}

#pragma mark - 每个CollectionView展示的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //在这里注册自定义的XIBcell否则会提示找不到标示符指定的cell
    //    UINib *nib = [UINib nibWithNibName:@"VIPCollectionViewCell"bundle: [NSBundle mainBundle]];
    //    [collectionView registerNib:nib forCellWithReuseIdentifier:cellId];
    
    
    HelpCenterCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    cell.imgName = imgArr[indexPath.row];
    cell.titleName = titleArr[indexPath.row];
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:{
            //加载wap页面
            WebPreViewController *webVC = [[WebPreViewController alloc]init];
            [self.navigationController pushViewController:webVC animated:YES];
        }
            break;
        case 1:{
            //加载wap页面
            ProductCenterViewController *productCenter = [[ProductCenterViewController alloc]init];
            [self.navigationController pushViewController:productCenter animated:YES];
        }
            break;
        case 2:{
            SomeQuesViewController *someQ = [[SomeQuesViewController alloc]init];
            [self.navigationController pushViewController:someQ animated:YES];
        }
            break;
        case 3:{
            ContactServiceViewController *service = [[ContactServiceViewController alloc]init];
            [self.navigationController pushViewController:service animated:YES];
            
        }
            break;
            
        default:
            break;
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
