//
//  MainTableViewCell.h
//  RecycalNew
//
//  Created by apple on 2017/8/3.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GuessLikeModel.h"
@interface MainTableViewCell : UITableViewCell

@property (nonatomic,strong)GuessLikeModel *model;

@end
