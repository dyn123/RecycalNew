//
//  MainTableViewCell.m
//  RecycalNew
//
//  Created by apple on 2017/8/3.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "MainTableViewCell.h"
@interface MainTableViewCell()
{
    
    __weak IBOutlet UILabel *category;
    __weak IBOutlet UILabel *address;
    __weak IBOutlet UILabel *endTime;
    __weak IBOutlet UILabel *publicTime;
    __weak IBOutlet UILabel *title;
}
@end
@implementation MainTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(GuessLikeModel *)model{
    
    title.text = model.D_Title;
    publicTime.text = model.D_PubTime;
    endTime.text = model.D_EndTime;
    //读取分类
    ReadAndWriteDateTools *t = [ReadAndWriteDateTools tools];
    int clas = [model.D_Class intValue];
    int group = [model.D_Group intValue];
    NSString *plistindex ; //第几个plist
    if (group== 0) {
        plistindex = [NSString stringWithFormat:@"%d",(clas-1)*2];
    }else{
        plistindex = [NSString stringWithFormat:@"%d",clas*2-1];
    }

    category.text =  [t readCateWithFirstCate:model.D_CidA SecondCate:nil ThirCate:nil plistName:plistindex][0];
    
    //读取地区
    NSMutableArray *addressArr = [t readAllAreaWithProvCode:model.D_ProvinceCode CityCode:model.D_CityCode AreaCode:nil Plist:kPCAPlist];
    address.text = [NSString stringWithFormat:@"%@-%@",addressArr[0],addressArr[1]];
}
@end
