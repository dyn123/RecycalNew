//
//  MainTableHeaderView.m
//  RecycalNew
//
//  Created by apple on 2017/8/3.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "MainTableHeaderView.h"


#import "SupplyListViewController.h"
#import "AssetListViewController.h"
#import "AuctionListViewController.h"
#import "RentListViewController.h"
@interface MainTableHeaderView()<UIScrollViewDelegate>
{
    UIPageControl *_pageCtrl;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *itemSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnSpace;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnWidth;

@property (weak, nonatomic) IBOutlet UIScrollView *headerScroll;

@end
#define ItemWidth 43
#define BtnWidth 118
@implementation MainTableHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
   
    _itemSpace.constant = (DEVICE_WIDTH - ItemWidth*4)/5;
    _btnSpace.constant = (DEVICE_WIDTH - BtnWidth*3)/4;
    _btnWidth.constant = BtnWidth;
    
}

- (void)layoutSubviews{
    [super layoutSubviews];

    
    _headerScroll.delegate = self;
    _headerScroll.contentSize = CGSizeMake(DEVICE_WIDTH*2, 0);
    _headerScroll.pagingEnabled = YES;
    _headerScroll.bounces = YES;
    _headerScroll.showsHorizontalScrollIndicator = NO;
    _headerScroll.showsVerticalScrollIndicator = NO;
    
    
    for (int i =0; i < 2; i++) {
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(i*DEVICE_WIDTH, 0, DEVICE_WIDTH, _headerScroll.frame.size.height)];
        [_headerScroll addSubview:imgView];
        imgView.image = [UIImage imageNamed:@"mainScroll_pic"];
    }
    
    
    
    //pageController
    
    _pageCtrl = [[UIPageControl alloc]initWithFrame:CGRectMake(0, _headerScroll.bounds.size.height-20, DEVICE_WIDTH, 20)];
    [self addSubview:_pageCtrl];
    
    _pageCtrl.currentPage     = 0;
    _pageCtrl.numberOfPages   = 2;
    _pageCtrl.pageIndicatorTintColor = [UIColor whiteColor];
    _pageCtrl.currentPageIndicatorTintColor = RGB(231, 0, 18, 1);
    [_pageCtrl addTarget:self action:@selector(pageTurn:) forControlEvents:UIControlEventValueChanged];
    

}


- (IBAction)itemBtnClick:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag-100) {
        case 0:{
            [self.viewController.navigationController pushViewController:[SupplyListViewController new] animated:YES];
        }
            break;
        case 1:{
             [self.viewController.navigationController pushViewController:[AssetListViewController new] animated:YES];
        }
            break;
        case 2:{
             [self.viewController.navigationController pushViewController:[AuctionListViewController new] animated:YES];
        }
            break;
        case 3:{
             [self.viewController.navigationController pushViewController:[RentListViewController new] animated:YES];
        }
            break;
            
        default:
            break;
    }
}


#pragma mark - scroll&page
- (void)pageTurn:(UIPageControl *)sender{
    
    [_headerScroll setContentOffset:CGPointMake(sender.currentPage * DEVICE_WIDTH, 0) animated:YES];
    
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    _pageCtrl.currentPage = scrollView.contentOffset.x/DEVICE_WIDTH;
}


@end
