//
//  MainViewController.m
//  RecycalNew
//
//  Created by apple on 2017/7/6.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "MainViewController.h"

#import "MainTableHeaderView.h"
#import "CustomSearchBar.h"
#import "MainTableViewCell.h"

#import "SearchViewController.h"

#import "GuessLikeModel.h"


//#define PAGE_SIZE 8

@interface MainViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UIScrollViewDelegate>
{
    //判断是否加载完成
    BOOL isLoadIOver;
    long nowPindex;//页码
    NSMutableArray *guessLikeArray; //猜你喜欢
}

@property (weak, nonatomic) IBOutlet UIView *searchBar;

@property (nonatomic,strong)UITableView *tabView;

@end


@implementation MainViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    
    //搜索条
    [self layoutSearchBar:_searchBar.frame];
    [self addTableView];
    
    if (guessLikeArray == nil) {
        guessLikeArray = [NSMutableArray array];
        nowPindex = 0;
        [_tabView reloadData];
       // [_tabView.mj_footer beginRefreshing];
    }
  
    
}


- (void)mjRefreshMethod{
    
    _tabView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadRefreshDownData)];
    
    
//    [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadRefreshDownData)];
    
    
//    _tabView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadRefreshUpData)];
    
    
    
    
}


#pragma mark - 上拉刷新
- (void)loadRefreshUpData{
    [self loadRefreshDownData];
}
#pragma mark - 加载列表
- (void)loadRefreshDownData{
    
    NSString *page = [NSString stringWithFormat:@"%ld",nowPindex];
    //猜你喜欢网络请求
    [Engine getGuessYouLikeWithCida:nil PageSize:@"8" Page:page Uid:nil Success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        NSArray *arr = [dic objectForKey:@"Item3"];
        NSMutableArray *modelArr = [GuessLikeModel mj_objectArrayWithKeyValuesArray:arr];
        [self addNewDataWithArr:modelArr];
        [_tabView reloadData];
        
    } Error:^(NSError *error) {
        
    }];
    
    
    
}


- (void)addNewDataWithArr:(NSMutableArray *)arr{
    
    for (int i =0; i<arr.count; i++) {
        [guessLikeArray addObject:arr[i]];
    }
    
}




- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    self.tabBarController.tabBar.hidden = NO;
}
- (void)addTableView{
    UITableView *tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, DEVICE_WIDTH, DEVICE_HEIGHT-64-35) style:UITableViewStyleGrouped];
    [self.view addSubview:tabView];
    tabView.tableHeaderView = [self HeaderView];
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tabView.delegate = self;
    tabView.dataSource = self;
    _tabView = tabView;
    
    [self mjRefreshMethod];
    
}




- (UIView *)HeaderView{
    MainTableHeaderView *tabHeader = [[[NSBundle mainBundle]loadNibNamed:@"MainTableHeaderView" owner:nil options:nil]lastObject];
    tabHeader.frame = CGRectMake(0, 0, DEVICE_WIDTH, 343);
    return tabHeader;
}
#pragma mark - tabViewData
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return  5;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 95;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return guessLikeArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell =[[[NSBundle mainBundle] loadNibNamed:@"MainTableViewCell" owner:self options:nil]lastObject];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }
    cell.model = guessLikeArray[indexPath.section];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
}

#pragma mark - 添加搜索条

- (void)layoutSearchBar:(CGRect)frame{

    CustomSearchBar *searchBar = [[CustomSearchBar alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    searchBar.delegate = self;
    [_searchBar addSubview:searchBar];
}

#pragma mark - 搜索条delegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    SearchViewController *searchVC = [[SearchViewController alloc]init];
    [self.navigationController pushViewController:searchVC animated:YES];
   // [self.searchBar resignFirstResponder];
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y + (scrollView.frame.size.height) > scrollView.contentSize.height) {
        [_tabView.mj_footer beginRefreshing];
    }
}

@end
