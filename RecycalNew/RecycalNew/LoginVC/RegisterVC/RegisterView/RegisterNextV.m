//
//  RegisterNextV.m
//  RecycalNew
//
//  Created by apple on 2017/10/21.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "RegisterNextV.h"
#import "RegisterSuccessViewController.h"
@implementation RegisterNextV

- (IBAction)nextBtnClick:(id)sender {
    [self.viewController.navigationController pushViewController:[RegisterSuccessViewController new] animated:YES];
}


@end
