//
//  RegisterViewController.m
//  RecycalNew
//
//  Created by apple on 2017/7/17.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "RegisterViewController.h"
#import "RegisterV.h"
@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"注册";
    [self addAllSubView];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
}
- (void)addAllSubView{
    RegisterV *registV = [[[NSBundle mainBundle]loadNibNamed:@"RegisterV" owner:self options:nil]lastObject];
    registV.frame = CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT);
    NSLog(@"==%f==%f",DEVICE_WIDTH,DEVICE_HEIGHT);
    [self.view addSubview:registV];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
