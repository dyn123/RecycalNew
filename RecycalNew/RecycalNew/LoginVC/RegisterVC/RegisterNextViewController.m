//
//  RegisterNextViewController.m
//  RecycalNew
//
//  Created by apple on 2017/10/21.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "RegisterNextViewController.h"
#import "RegisterNextV.h"
@interface RegisterNextViewController ()

@end

@implementation RegisterNextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"注册";
    [self addAllSubView];
}
- (void)addAllSubView{
    RegisterNextV *registNextV = [[[NSBundle mainBundle]loadNibNamed:@"RegisterNextV" owner:self options:nil]lastObject];
    registNextV.frame = CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT);
    NSLog(@"==%f==%f",DEVICE_WIDTH,DEVICE_HEIGHT);
    [self.view addSubview:registNextV];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
