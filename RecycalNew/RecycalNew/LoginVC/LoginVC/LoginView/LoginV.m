//
//  LoginView.m
//  RecycalNew
//
//  Created by apple on 2017/10/21.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "LoginV.h"
#import "LoginPhoneViewController.h"
#import "ForgetFirstViewController.h"
#import "RegisterViewController.h"
@implementation LoginV
- (IBAction)loginPhoneBtnClick:(id)sender {
    
    [self.viewController.navigationController pushViewController:[LoginPhoneViewController new] animated:YES];
}
- (IBAction)forgetBtnClick:(id)sender {
    [self.viewController.navigationController pushViewController:[ForgetFirstViewController new] animated:YES];
}

- (IBAction)registerBtnClick:(id)sender {
     [self.viewController.navigationController pushViewController:[RegisterViewController new] animated:YES];
}


@end
