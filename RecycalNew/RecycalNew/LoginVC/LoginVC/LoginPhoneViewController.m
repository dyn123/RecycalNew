//
//  LoginPhoneViewController.m
//  RecycalNew
//
//  Created by apple on 2017/10/21.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "LoginPhoneViewController.h"
#import "LoginPhoneV.h"
@interface LoginPhoneViewController ()

@end

@implementation LoginPhoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"手机动态登录";
    [self addAllSubView];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
}
- (void)addAllSubView{
    LoginPhoneV *phoneV = [[[NSBundle mainBundle]loadNibNamed:@"LoginPhoneV" owner:self options:nil]lastObject];
    phoneV.frame = CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT);
    NSLog(@"==%f==%f",DEVICE_WIDTH,DEVICE_HEIGHT);
    [self.view addSubview:phoneV];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
