//
//  AuctionInfoViewController.m
//  RecycalNew
//
//  Created by apple on 2017/8/7.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "AuctionInfoViewController.h"
#import "AuctionInfoHeadView.h"
#import "AuctionInfoContactView.h"
#import "AuctionInfoContentView.h"

#define HEAD_HEIGHT 424

#define CONTACT_HEIGHT 202
#define CONTENT_HEIGHT 500


@interface AuctionInfoViewController ()
@property (nonatomic,strong)UIScrollView *bgScrol;
@end

@implementation AuctionInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"拍卖信息详情";
    
    _bgScrol = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT)];
    [self.view addSubview:_bgScrol];
    _bgScrol.contentSize = CGSizeMake(0, 1100);
    [self addHeadInfoView];
    [self addContactView];
    [self addContentView];
}

- (void)addHeadInfoView{
    
    AuctionInfoHeadView *headView = [[[NSBundle mainBundle]loadNibNamed:@"AuctionInfoHeadView" owner:nil options:nil]lastObject];
    headView.frame = CGRectMake(0, 0, DEVICE_WIDTH, HEAD_HEIGHT);
    [_bgScrol addSubview:headView];
    
}

- (void)addContactView{
    AuctionInfoContactView *contactView = [[[NSBundle mainBundle]loadNibNamed:@"AuctionInfoContactView" owner:nil options:nil]lastObject];
    contactView.frame = CGRectMake(0, HEAD_HEIGHT, DEVICE_WIDTH, CONTACT_HEIGHT);
    [_bgScrol addSubview:contactView];
}

- (void)addContentView{
    AuctionInfoContentView *webView = [[[NSBundle mainBundle]loadNibNamed:@"AuctionInfoContentView" owner:nil options:nil]lastObject];
    webView.frame = CGRectMake(0, HEAD_HEIGHT+CONTACT_HEIGHT, DEVICE_WIDTH, CONTENT_HEIGHT);
    [_bgScrol addSubview:webView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
