//
//  AreaModel.m
//  RecycalNew
//
//  Created by apple on 2017/8/22.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "AreaModel.h"

@implementation AreaModel
/*
 ng *A_C_Code;
 @property (strong,nonatomic)NSString *A_Code;
 @property (strong,nonatomic)NSString *A_Id;
 @property (strong,nonatomic)NSString *A_Name;
 @property (strong,nonatomic)NSString *A_Pinyin;
 */
#pragma -mark将对象转化为NSData的方法
-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.A_C_Code forKey:@"A_C_Code"];
    [aCoder encodeObject:self.A_Code forKey:@"A_Code"];
    [aCoder encodeObject:self.A_Id forKey:@"A_Id"];
    [aCoder encodeObject:self.A_Name forKey:@"A_Name"];
    [aCoder encodeObject:self.A_Pinyin forKey:@"A_Pinyin"];

    
}
-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ([self init]) {
        //解压过程
        self.A_C_Code= [aDecoder decodeObjectForKey:@"A_C_Code"];
        self.A_Code= [aDecoder decodeObjectForKey:@"A_Code"];
        self.A_Id= [aDecoder decodeObjectForKey:@"A_Id"];
        self.A_Name= [aDecoder decodeObjectForKey:@"A_Name"];
        self.A_Pinyin= [aDecoder decodeObjectForKey:@"A_Pinyin"];
       

        
    }
    return self;
    
}


+(NSMutableArray *)initWithDataArray:(NSArray *)dataArray{
    NSMutableArray *arr = [NSMutableArray array];
    for (NSDictionary *dic in dataArray) {
        AreaModel *p = [[AreaModel alloc]init];
        
//        p.A_C_Code = [dic objectForKey:@"A_C_Code"];
//        p.A_Code = [dic objectForKey:@"A_Code"];
        
        NSNumber *ACCode = [dic objectForKey:@"A_C_Code"];
        p.A_C_Code = [ACCode stringValue];
        
        NSNumber *ACode = [dic objectForKey:@"A_Code"];
        p.A_Code = [ACode stringValue];
        
        
        
        p.A_Id = [dic objectForKey:@"A_Id"];
        p.A_Name = [dic objectForKey:@"A_Name"];
        p.A_Pinyin = [dic objectForKey:@"A_Pinyin"];
        
        [arr addObject:p];
        
    }
    
    return arr;
}


@end
