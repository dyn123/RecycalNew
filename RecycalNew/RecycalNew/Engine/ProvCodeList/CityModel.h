//
//  CityModel.h
//  RecycalNew
//
//  Created by apple on 2017/8/22.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityModel : NSObject<NSCoding>

@property (strong,nonatomic)NSString *C_Code;
@property (strong,nonatomic)NSString *C_Id;
@property (strong,nonatomic)NSString *C_Name;
@property (strong,nonatomic)NSString *C_OpenWebsite;
@property (strong,nonatomic)NSString *C_P_Code;
@property (strong,nonatomic)NSString *C_Pinyin;
@property (strong,nonatomic)NSDictionary *areasDic;

+(NSMutableArray *)initWithDataArray:(NSArray *)dataArray;
@end
