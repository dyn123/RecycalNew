//
//  CityModel.m
//  RecycalNew
//
//  Created by apple on 2017/8/22.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "CityModel.h"

@implementation CityModel

#pragma -mark将对象转化为NSData的方法

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.C_Code forKey:@"C_Code"];
    [aCoder encodeObject:self.C_Id forKey:@"C_Id"];
    [aCoder encodeObject:self.C_Name forKey:@"C_Name"];
    [aCoder encodeObject:self.C_OpenWebsite forKey:@"C_OpenWebsite"];
    [aCoder encodeObject:self.C_P_Code forKey:@"C_P_Code"];
    [aCoder encodeObject:self.C_Pinyin forKey:@"C_Pinyin"];
    [aCoder encodeObject:self.areasDic forKey:@"areasDic"];
    
}
-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ([self init]) {
        //解压过程
        self.C_Code= [aDecoder decodeObjectForKey:@"C_Code"];
        self.C_Id= [aDecoder decodeObjectForKey:@"C_Id"];
        self.C_Name= [aDecoder decodeObjectForKey:@"C_Name"];
        self.C_OpenWebsite= [aDecoder decodeObjectForKey:@"C_OpenWebsite"];
        self.C_P_Code= [aDecoder decodeObjectForKey:@"C_P_Code"];
        self.C_Pinyin= [aDecoder decodeObjectForKey:@"C_Pinyin"];
        self.areasDic = [aDecoder decodeObjectForKey:@"areasDic"];

    }
    return self;
    
}

+(NSMutableArray *)initWithDataArray:(NSArray *)dataArray{
    NSMutableArray *arr = [NSMutableArray array];
    for (NSDictionary *dic in dataArray) {
        CityModel *p = [[CityModel alloc]init];
        
        NSNumber *pCode = [dic objectForKey:@"C_Code"];
        p.C_Code = [pCode stringValue];
        
        
//        p.C_P_Code = [dic objectForKey:@"C_P_Code"];
        
        NSNumber *CPCode = [dic objectForKey:@"C_P_Code"];
        p.C_P_Code = [CPCode stringValue];
        
        
        
        p.C_Id = [dic objectForKey:@"C_Id"];
        p.C_Name = [dic objectForKey:@"C_Name"];
        p.C_OpenWebsite = [dic objectForKey:@"C_OpenWebsite"];
        
        p.C_Pinyin = [dic objectForKey:@"C_Pinyin"];
        p.areasDic = [NSMutableDictionary dictionary];
        
        [arr addObject:p];
        
    }
    
    return arr;
}


@end
