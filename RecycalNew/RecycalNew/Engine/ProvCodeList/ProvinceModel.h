//
//  ProvinceModel.h
//  RecycalNew
//
//  Created by apple on 2017/8/22.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProvinceModel : NSObject<NSCoding>

@property (strong,nonatomic)NSString *P_Code;
@property (strong,nonatomic)NSString *P_Id;
@property (strong,nonatomic)NSString *P_Name;
@property (strong,nonatomic)NSString *P_Pinyin;
@property (strong,nonatomic)NSString *P_SName;
@property (strong,nonatomic)NSDictionary *citysDic;

+(id)initWithDataArray:(NSArray *)dataArray;
@end
