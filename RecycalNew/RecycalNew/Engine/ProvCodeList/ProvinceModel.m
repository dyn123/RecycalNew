//
//  ProvinceModel.m
//  RecycalNew
//
//  Created by apple on 2017/8/22.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "ProvinceModel.h"

@implementation ProvinceModel



#pragma -mark将对象转化为NSData的方法
-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.P_Code forKey:@"P_Code"];
    [aCoder encodeObject:self.P_Id forKey:@"P_Id"];
    [aCoder encodeObject:self.P_Name forKey:@"P_Name"];
    [aCoder encodeObject:self.P_Pinyin forKey:@"P_Pinyin"];
    [aCoder encodeObject:self.P_SName forKey:@"P_SName"];
    [aCoder encodeObject:self.citysDic forKey:@"citysDic"];
    
}
-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ([self init]) {
        //解压过程
        self.P_Code= [aDecoder decodeObjectForKey:@"P_Code"];
        self.P_Id= [aDecoder decodeObjectForKey:@"P_Id"];
        self.P_Name= [aDecoder decodeObjectForKey:@"P_Name"];
        self.P_Pinyin= [aDecoder decodeObjectForKey:@"P_Pinyin"];
        self.P_SName= [aDecoder decodeObjectForKey:@"P_SName"];
        self.citysDic = [aDecoder decodeObjectForKey:@"citysDic"];
    }
    return self;
    
}
+(NSMutableArray *)initWithDataArray:(NSArray *)dataArray{
    NSMutableArray *arr = [NSMutableArray array];
    for (NSDictionary *dic in dataArray) {
        ProvinceModel *p = [[ProvinceModel alloc]init];
        
        NSNumber *pCode = [dic objectForKey:@"P_Code"];
        p.P_Code = [pCode stringValue];
        
        p.P_Id = [dic objectForKey:@"P_Id"];
        p.P_Name = [dic objectForKey:@"P_Name"];
        p.P_Pinyin = [dic objectForKey:@"P_Pinyin"];
        p.P_SName = [dic objectForKey:@"P_SName"];
        p.citysDic = [NSMutableDictionary dictionary];
        [arr addObject:p];
         
    }
    
    return arr;
}

@end
