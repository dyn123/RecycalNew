//
//  AreaModel.h
//  RecycalNew
//
//  Created by apple on 2017/8/22.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AreaModel : NSObject

@property (strong,nonatomic)NSString *A_C_Code;
@property (strong,nonatomic)NSString *A_Code;
@property (strong,nonatomic)NSString *A_Id;
@property (strong,nonatomic)NSString *A_Name;
@property (strong,nonatomic)NSString *A_Pinyin;

+(NSMutableArray *)initWithDataArray:(NSArray *)dataArray;

@end
