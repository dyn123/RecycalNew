//
//  CategoryListModel.m
//  RecycalNew
//
//  Created by apple on 2017/8/23.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "CategoryListModel.h"

@implementation CategoryListModel



#pragma -mark将对象转化为NSData的方法
-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.Ca_FatherId forKey:@"Ca_FatherId"];
    [aCoder encodeObject:self.Ca_First forKey:@"Ca_First"];
    [aCoder encodeObject:self.Ca_Group forKey:@"Ca_Group"];
    [aCoder encodeObject:self.Ca_Id forKey:@"Ca_Id"];
    [aCoder encodeObject:self.Ca_MydataId forKey:@"Ca_MydataId"];
    [aCoder encodeObject:self.Ca_Name forKey:@"Ca_Name"];
    [aCoder encodeObject:self.Ca_OpenWebSite forKey:@"Ca_OpenWebSite"];
    [aCoder encodeObject:self.Ca_Order forKey:@"Ca_Order"];
    [aCoder encodeObject:self.Ca_Pinyin forKey:@"Ca_Pinyin"];
    [aCoder encodeObject:self.Ca_Show forKey:@"Ca_Show"];
    [aCoder encodeObject:self.Cd_Ca_Id forKey:@"Cd_Ca_Id"];
    [aCoder encodeObject:self.Cd_Class forKey:@"Cd_Class"];
    [aCoder encodeObject:self.Cd_Id forKey:@"Cd_Id"];
    [aCoder encodeObject:self.Cd_Order forKey:@"Cd_Order"];
    [aCoder encodeObject:self.Cd_Status forKey:@"Cd_Status"];
    [aCoder encodeObject:self.nextList forKey:@"nextList"];
}
-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ([self init]) {
        //解压过程
        self.Ca_FatherId= [aDecoder decodeObjectForKey:@"Ca_FatherId"];
        self.Ca_First= [aDecoder decodeObjectForKey:@"Ca_First"];
        self.Ca_Group= [aDecoder decodeObjectForKey:@"Ca_Group"];
        self.Ca_Id= [aDecoder decodeObjectForKey:@"Ca_Id"];
        self.Ca_MydataId= [aDecoder decodeObjectForKey:@"Ca_MydataId"];
        self.Ca_Name= [aDecoder decodeObjectForKey:@"Ca_Name"];
        self.Ca_OpenWebSite= [aDecoder decodeObjectForKey:@"Ca_OpenWebSite"];
        self.Ca_Order= [aDecoder decodeObjectForKey:@"Ca_Order"];
        self.Ca_Pinyin= [aDecoder decodeObjectForKey:@"Ca_Pinyin"];
        self.Ca_Show= [aDecoder decodeObjectForKey:@"Ca_Show"];
        self.Cd_Ca_Id= [aDecoder decodeObjectForKey:@"Cd_Ca_Id"];
        self.Cd_Class= [aDecoder decodeObjectForKey:@"Cd_Class"];
        
        self.Cd_Id= [aDecoder decodeObjectForKey:@"Cd_Id"];
        self.Cd_Order= [aDecoder decodeObjectForKey:@"Cd_Order"];
        self.Cd_Status= [aDecoder decodeObjectForKey:@"Cd_Status"];
        self.nextList= [aDecoder decodeObjectForKey:@"nextList"];

    }
    return self;
    
}

/*
 "Ca_FatherId" = 2249;
 "Ca_First" = F;
 "Ca_Group" = 2;
 "Ca_Id" = 2250;
 "Ca_MydataId" = 801;
 "Ca_Name" = "\U5e9f\U5408\U6210\U5316\U7ea4";
 "Ca_OpenWebSite" = 0;
 "Ca_Order" = 1;
 "Ca_Pinyin" = feigechenghuaqian;
 "Ca_Show" = 1;
 "Cd_Ca_Id" = 2250;
 "Cd_Class" = 1;
 "Cd_Id" = 2249;
 "Cd_Order" = 1;
 "Cd_Status" = 1;
 */


+(id)initWithDic:(NSDictionary *)dic{

    CategoryListModel *p = [[CategoryListModel alloc]init];
        
    
    p.Ca_Id = [dic objectForKey:@"Ca_Id"];
        
    p.Ca_FatherId = [dic objectForKey:@"Ca_FatherId"];
    p.Ca_First = [dic objectForKey:@"Ca_First"];
    p.Ca_Group = [dic objectForKey:@"Ca_Group"];
    p.Ca_MydataId = [dic objectForKey:@"Ca_MydataId"];
    p.Ca_Name = [dic objectForKey:@"Ca_Name"];
    p.Ca_OpenWebSite = [dic objectForKey:@"Ca_OpenWebSite"];
    p.Ca_Order = [dic objectForKey:@"Ca_Order"];
    p.Ca_Pinyin = [dic objectForKey:@"Ca_Pinyin"];
    p.Ca_Show = [dic objectForKey:@"Ca_Show"];
    
    
    NSNumber *c_Ca_Id = [dic objectForKey:@"Cd_Ca_Id"];
    p.Cd_Ca_Id = [c_Ca_Id stringValue];
    
    
    p.Cd_Class = [dic objectForKey:@"Cd_Class"];
    p.Cd_Id = [dic objectForKey:@"Cd_Id"];
    p.Cd_Order = [dic objectForKey:@"Cd_Order"];
    p.Cd_Status = [dic objectForKey:@"Cd_Status"];
       
    p.nextList = [NSMutableDictionary dictionary];
        
        
 
        

    
    return p;
}




@end
