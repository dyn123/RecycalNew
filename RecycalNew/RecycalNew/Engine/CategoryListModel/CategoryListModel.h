//
//  CategoryListModel.h
//  RecycalNew
//
//  Created by apple on 2017/8/23.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryListModel : NSObject<NSCoding>

@property (strong,nonatomic)NSString *Ca_FatherId;
@property (strong,nonatomic)NSString *Ca_First;
@property (strong,nonatomic)NSString *Ca_Group;
@property (strong,nonatomic)NSString *Ca_Id;
@property (strong,nonatomic)NSString *Ca_MydataId;
@property (strong,nonatomic)NSString *Ca_Name;
@property (strong,nonatomic)NSString *Ca_OpenWebSite;
@property (strong,nonatomic)NSString *Ca_Order;
@property (strong,nonatomic)NSString *Ca_Pinyin;
@property (strong,nonatomic)NSString *Ca_Show;
@property (strong,nonatomic)NSString *Cd_Ca_Id;
@property (strong,nonatomic)NSString *Cd_Class;
@property (strong,nonatomic)NSString *Cd_Id;
@property (strong,nonatomic)NSString *Cd_Order;
@property (strong,nonatomic)NSString *Cd_Status;
@property (strong,nonatomic)NSMutableDictionary *nextList;

+(id)initWithDic:(NSDictionary *)dic;
@end
