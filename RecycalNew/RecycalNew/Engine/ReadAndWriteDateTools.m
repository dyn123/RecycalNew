//
//  ReadAndWriteDateTools.m
//  RecycalNew
//
//  Created by apple on 2017/8/23.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "ReadAndWriteDateTools.h"




#import "ProvinceModel.h"
#import "CityModel.h"
#import "AreaModel.h"


#import "CategoryListModel.h"




@interface ReadAndWriteDateTools()

//@property (nonatomic,strong)NSArray *plistArray;

@end


@implementation ReadAndWriteDateTools

static ReadAndWriteDateTools * _tool = nil;
static NSArray *_plists = nil;

+ (ReadAndWriteDateTools *)tools {
    
    
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        _tool = [[ReadAndWriteDateTools alloc] init];
        _plists = @[@"gongFCATEPlist.plist",
                             @"gongSCATEPlist.plist",
                             @"qiuFCATEQPlist.plist",
                             @"qiuSCATEQPlist.plist",
                             @"zuFCATEQPlist.plist",
                             @"zuSCATEQPlist.plist",
                             @"paiFCATEQPlist.plist",
                             @"paiSCATEQPlist.plist",
                             @"qiyeFCATEQPlist.plist",
                             @"qiyeSCATEQPlist.plist",
                             @"zichanFCATEQPlist.plist",
                             @"zichanSCATEQPlist.plist",
                             @"zhanhuiFCATEQPlist.plist",
                             @"zhanhuiSCATEQPlist.plist" ,
                             @"shichangFCATEQPlist.plist",
                             @"shichangSCATEQPlist.plist" ,
                             @"zixunFCATEQPlist.plist" ,
                             @"zixunSCATEQPlist.plist" ];
        
    });
    
    
    
    return _tool;
    
}


#pragma mark 请求分类数据
- (void)writeAllCategory{
    //1供应 二手设备0  再生资源2
    //2求购 二手设备  再生资源
    //3租赁 二手设备  再生资源
    //4拍卖 二手设备  再生资源
    //5企业报价 二手设备  再生资源
    //6资产 二手设备  再生资源
    //7展会 二手设备  再生资源
    //8市场 二手设备  再生资源
    //9资讯 二手设备  再生资源
    /*
    NSArray *plistArr = @[@"gongFCATEPlist.plist",
                          @"gongSCATEPlist.plist",
                          @"qiuFCATEQPlist.plist",
                          @"qiuSCATEQPlist.plist",
                          @"zuFCATEQPlist.plist",
                          @"zuSCATEQPlist.plist",
                          @"paiFCATEQPlist.plist",
                          @"paiSCATEQPlist.plist",
                          @"qiyeFCATEQPlist.plist",
                          @"qiyeSCATEQPlist.plist",
                          @"zichanFCATEQPlist.plist",
                          @"zichanSCATEQPlist.plist",
                          @"zhanhuiFCATEQPlist.plist",
                          @"zhanhuiSCATEQPlist.plist" ,
                          @"shichangFCATEQPlist.plist",
                          @"shichangSCATEQPlist.plist" ,
                          @"zixunFCATEQPlist.plist" ,
                          @"zixunSCATEQPlist.plist" ];
    */
    
    for (int i=1; i<10; i++) {
        NSString *dclass = [NSString stringWithFormat:@"%d",i];
        [Engine getCateByClassWithDClass:dclass Group:@"0" Success:^(NSDictionary *dic) {
            
            [self getCategoryWithDic:dic plist:_plists[(i-1)*2]];
        } Error:^(NSError *error) {
            
        }];
        [Engine getCateByClassWithDClass:dclass Group:@"2" Success:^(NSDictionary *dic) {
            
            [self getCategoryWithDic:dic plist:_plists[i*2-1]];
        } Error:^(NSError *error) {
            
        }];
        
    }
    
    
    
}

#pragma mark -写入分类类别
- (void)getCategoryWithDic:(NSDictionary *)dic plist:(NSString *)plist{
    
    NSArray *cateArr = [dic objectForKey:@"Item3"];
    NSMutableDictionary *cateDic = [NSMutableDictionary dictionary];
    
    for (NSDictionary *firstDic in cateArr) {
        NSDictionary *cateM = [firstDic objectForKey:@"CateModel"];
        CategoryListModel *category1 = [CategoryListModel initWithDic:cateM];
        
        //二级数据
        NSArray *secArr = [firstDic objectForKey:@"CateSecList"];
        if(secArr.count>0){
            
            for (NSDictionary *secDic in secArr) {
                
                NSDictionary *cateM = [secDic objectForKey:@"CateModel"];
                CategoryListModel *category2 = [CategoryListModel initWithDic:cateM];
                
                
                //三级数据
                NSArray *thrArr = [secDic objectForKey:@"CateSecList"];
                if(thrArr.count>0){
                    for (NSDictionary *thrDic in thrArr) {
                        
                        NSDictionary *cateM = [thrDic objectForKey:@"CateModel"];
                        CategoryListModel *category3 = [CategoryListModel initWithDic:cateM];
                        
                        [category2.nextList setValue:category3 forKey:category3.Cd_Ca_Id];
                        
                    }
                }
                [category1.nextList setValue:category2 forKey:category2.Cd_Ca_Id];
            }
        }
        [cateDic setValue:category1 forKey:category1.Cd_Ca_Id];
        
        
    }
    
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:cateDic];
    [data writeToFile:[self getPlistPathWithPlist:plist] atomically:YES];
    
    
    
}
#pragma mark - 读取分类信息
- (NSMutableArray *)readCateWithFirstCate:(NSString *)firstCate SecondCate:(NSString *)secCate ThirCate:(NSString *)thirCate plistName:(NSString *)plist{
    
    NSMutableArray *returnArr = [NSMutableArray array];
    //读取数据
    NSInteger i = [plist intValue];
    NSString *path = [self getPlistPathWithPlist:_plists[i]];
    NSData *someData = [[NSData alloc]initWithContentsOfFile:path];
    NSMutableDictionary  *dict = [NSKeyedUnarchiver unarchiveObjectWithData:someData];
    
    
    if (firstCate) {
        
        CategoryListModel *p = (CategoryListModel *)[dict objectForKey:firstCate];
        if (p) {
            
            [returnArr addObject:p.Ca_Name];
            
            if (secCate) {
                CategoryListModel *c = [p.nextList objectForKey:secCate];
                if (c) {
                    
                    [returnArr addObject:c.Ca_Name];
                    
                    if (thirCate) {
                        CategoryListModel *a = [c.nextList objectForKey:thirCate];
                        if (a) {
                             [returnArr addObject:a.Ca_Name];
                        }
                       
                    }
                    
                }
                
                
            }
            
        }
        

    }
    
    return returnArr;
    
    
    
//    NSLog(@"%@-%@-%@",p.Ca_Name, c.Ca_Name,a.Ca_Name);
    
    
}





#pragma mark 请求地区数据
- (void)writeAllArea{
    [Engine getAllAreaListSuccess:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        NSArray *provArr = [dic objectForKey:@"Item3"];
        NSArray *cityArr = [dic objectForKey:@"Item4"];
        NSArray *areaArr = [dic objectForKey:@"Item5"];
        [self writeCodeWithPlist:kPCAPlist provArr:provArr CityArr:cityArr AreaArr:areaArr];
    } Error:^(NSError *error) {
        
    }];
}




#pragma mark  - address ----写入数据
- (void)writeCodeWithPlist:(NSString *)plist provArr:(NSArray *)provArr CityArr:(NSArray *)cityArr AreaArr:(NSArray *)areaArr{
    NSMutableArray *provinceArray = [ProvinceModel initWithDataArray:provArr];
    NSMutableArray *cityArray = [CityModel initWithDataArray:cityArr];
    NSMutableArray *areaArray = [AreaModel initWithDataArray:areaArr];
    
    NSMutableDictionary *provDic = [NSMutableDictionary dictionary];
    
    for (ProvinceModel *prov in provinceArray) {
        for (CityModel *city in cityArray) {
            if([prov.P_Code isEqualToString: city.C_P_Code ]){
                //同一类市
                for (AreaModel *area in areaArray) {
                    if ([city.C_Code isEqualToString: area.A_C_Code]) {
                        //同一类县
                        [city.areasDic setValue:area forKey:area.A_Code];
                        // [city.areasDic setObject:area forKey:area.A_Code];
                        
                        NSLog(@"%@",city.areasDic);
                    }
                }
                [prov.citysDic setValue:city forKey:city.C_Code];
            }
        }
        [provDic setValue:prov forKey:prov.P_Code];
    }
    
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:provDic];
    [data writeToFile:[self getPlistPathWithPlist:plist] atomically:YES];
    
}


#pragma mark  area  ---- 读取哪个plist文件下的某code数据
- (NSMutableArray *)readAllAreaWithProvCode:(NSString *)provCode CityCode:(NSString *)cityCode AreaCode:(NSString *)areaCode Plist:(NSString *)plist{
    
    //读取数据
    //省  市   县
    NSString *path = [self getPlistPathWithPlist:plist];
    NSData *someData = [[NSData alloc]initWithContentsOfFile:path];
    NSMutableDictionary  *dic = [NSKeyedUnarchiver unarchiveObjectWithData:someData];
    
    NSMutableArray *returnArr = [NSMutableArray array];
    
    
    if (provCode) {
        
        ProvinceModel *p = (ProvinceModel *)[dic objectForKey:provCode];
        [returnArr addObject:p.P_Name];
        
        if (cityCode) {
            CityModel *c = [p.citysDic objectForKey:cityCode];
            [returnArr addObject:c.C_Name];
            
            if (areaCode) {
                AreaModel *a = [c.areasDic objectForKey:areaCode];
                [returnArr addObject:a.A_Name];
            }
            
        }
        
    }
    
    return returnArr;
    
}




#pragma mark - 创建文件
- (NSString *)getPlistPathWithPlist:(NSString *)plist{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);NSString * documentDirectory = [paths objectAtIndex:0];
    NSString *writePath = [documentDirectory stringByAppendingPathComponent:plist];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //判断文件是否存在
    if (![fileManager fileExistsAtPath:writePath]){
        //create the file
        [fileManager createFileAtPath:writePath contents:nil attributes:nil];
        
    }
    return writePath;
    
    
}




@end
