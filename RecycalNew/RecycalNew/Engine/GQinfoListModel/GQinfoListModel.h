//
//  GQinfoListModel.h
//  RecycalNew
//
//  Created by apple on 2017/8/15.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GQinfoListModel : NSObject


@property (nonatomic,copy)NSString *D_AuctionTime;
@property (nonatomic,copy)NSString *D_BeginTime;
@property (nonatomic,copy)NSString *D_CidA;
@property (nonatomic,copy)NSString *D_CidAName;
@property (nonatomic,copy)NSString *D_CidB;
@property (nonatomic,copy)NSString *D_CidBName;
@property (nonatomic,copy)NSString *D_CidC;
@property (nonatomic,copy)NSString *D_CidCName;
@property (nonatomic,copy)NSString *D_CityCode;
@property (nonatomic,copy)NSString *D_CityName;
@property (nonatomic,copy)NSString *D_Class;
@property (nonatomic,copy)NSString *D_CountryCode;
@property (nonatomic,copy)NSString *D_CountryName;
@property (nonatomic,copy)NSString *D_Deposit;
@property (nonatomic,copy)NSString *D_EndTime;
@property (nonatomic,copy)NSString *D_Id;
@property (nonatomic,copy)NSString *D_M_Id;
@property (nonatomic,copy)NSString *D_Pic;
@property (nonatomic,copy)NSString *D_ProvinceCode;
@property (nonatomic,copy)NSString *D_ProvinceName;
@property (nonatomic,copy)NSString *D_PubTime;
@property (nonatomic,copy)NSString *D_RefreshTime;
@property (nonatomic,copy)NSString *D_State;
@property (nonatomic,copy)NSString *D_Title;
@property (nonatomic,copy)NSString *M_Level;


@end
