//
//  Engine.h
//  RecycalNew
//
//  Created by apple on 2017/8/11.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^SuccessBlock)(NSDictionary *dic);
typedef void (^ErrorBlock)(NSError *error);
@interface Engine : NSObject


/*****************登录注册******************/
#pragma mark - 用户注册
+(void)postRegisterWithUserName:(NSString *)username  Password:(NSString *)password Mobile:(NSString *)mobile Company:(NSString *)company Contact:(NSString *)contact Prov:(NSString *)prov City:(NSString *)city Cate:(NSString *)cate Ip:(NSString *)Ip Success:(SuccessBlock)success Error:(ErrorBlock)error;
#pragma mark - 用户登录
+(void)postLoginWithName:(NSString *)name Password:(NSString *)password Ip:(NSString *)ip Success:(SuccessBlock)success Error:(ErrorBlock)error;

#pragma mark - 获取验证码
+(void)getSendCodeWithPhoneNum:(NSString *)phoneNum Type:(NSString *)type Mid:(NSString *)mid Success:(SuccessBlock)success Error:(ErrorBlock)error;


#pragma mark - 刷新token
+(void)getRefreshLogTimeWithToken:(NSString *)token Success:(SuccessBlock)success Error:(ErrorBlock)error;


#pragma mark - 验证验证码
+(void)getVerifyCodeWithPhoneNum:(NSString *)phoneNum  Type:(NSString *)type Code:(NSString *)code Success:(SuccessBlock)success Error:(ErrorBlock)error;



/*****************设置中心******************/
#pragma mark - 修改密码
+(void)postChangePWDWithMid:(NSString *)mid PwdStr1:(NSString *)pwdstr1 PwdStr2:(NSString *)pwdstr2 OldPwdStr:(NSString *)oldpwdstr Type:(NSString *)type Success:(SuccessBlock)success Error:(ErrorBlock)error;
/*****************首页******************/
#pragma mark - 猜你喜欢
+(void)getGuessYouLikeWithCida:(NSString *)cida PageSize:(NSString *)pagesize Page:(NSString *)page Uid:(NSString *)uid  Success:(SuccessBlock)success Error:(ErrorBlock)error;
#pragma mark - 供求列表
+(void)getGQinfoListWithCateA:(NSString *)cateA CateB:(NSString *)cateB CateC:(NSString *)cateC Pcode:(NSString *)pcode Ccode:(NSString *)ccode Acode:(NSString *)acode Mlevel:(NSString *)mlevel Title:(NSString *)title  Pagesize:(NSString *)pagesize Pageindex:(NSString *)pageindex Group:(NSString *)group Success:(SuccessBlock)success Error:(ErrorBlock)error;
#pragma mark - 资产列表
+(void)getAssetInfoListWithCateA:(NSString *)cateA CateB:(NSString *)cateB CateC:(NSString *)cateC Pcode:(NSString *)pcode Ccode:(NSString *)ccode Acode:(NSString *)acode Mlevel:(NSString *)mlevel Sesrch:(NSString *)search Orderby:(NSString *)orderby Pagesize:(NSString *)pagesize Pageindex:(NSString *)pageindex Group:(NSString *)group Date:(NSString *)date Success:(SuccessBlock)success Error:(ErrorBlock)error;

#pragma mark - 拍卖列表
+(void)getAuctionInfoListWithCateA:(NSString *)cateA CateB:(NSString *)cateB CateC:(NSString *)cateC Pcode:(NSString *)pcode Ccode:(NSString *)ccode Acode:(NSString *)acode Mlevel:(NSString *)mlevel SesrchKey:(NSString *)searchKey Pagesize:(NSString *)pagesize Pageindex:(NSString *)pageindex Group:(NSString *)group Date:(NSString *)date HasContent:(NSString *)hasContent Success:(SuccessBlock)success Error:(ErrorBlock)error;

#pragma mark - 租赁列表
+(void)getLeaseDataInfoListWithCateA:(NSString *)cateA CateB:(NSString *)cateB CateC:(NSString *)cateC Pcode:(NSString *)pcode Ccode:(NSString *)ccode Acode:(NSString *)acode InfoType:(NSString *)infotype Sesrch:(NSString *)search Pagesize:(NSString *)pagesize Pageindex:(NSString *)pageindex Success:(SuccessBlock)success Error:(ErrorBlock)error;

#pragma mark - 信息详情
+(void)getDataDetailModelWithDid:(NSString *)did DataType:(NSString *)datatype Uid:(NSString *)uid Success:(SuccessBlock)success Error:(ErrorBlock)error;

/***************个人中心*******************/
#pragma mark - 获取会员信息
+(void)getMemeberById:(NSString *)Id uccess:(SuccessBlock)success Error:(ErrorBlock)error;

#pragma mark - 浏览记录
+(void)getMemberBrowseLogWithDay:(NSString *)day Dclass:(NSString *)dclass Page:(NSString *)page Mid:(NSString *)mid PageSize:(NSString *)pagesize Success:(SuccessBlock)success Error:(ErrorBlock)error;
#pragma mark - 删除浏览记录
+(void)getDelBrowseLogWithDid:(NSString *)did Success:(SuccessBlock)success Error:(ErrorBlock)error;

#pragma mark - 收藏信息
+(void)getSetCollectWithDid:(NSString *)did Mid:(NSString *)mid Success:(SuccessBlock)success Error:(ErrorBlock)error;
#pragma mark - 取消收藏
+(void)getUnCollectWithDid:(NSString *)did Mid:(NSString *)mid Success:(SuccessBlock)success Error:(ErrorBlock)error;


#pragma mark - 我的推送
+(void)GetRecommendInfoListWithDataClass:(NSString *)dataclass PageIndex:(NSString *)pageindex PageSize:(NSString *)pagesize Mid:(NSString *)mid Title:(NSString *)title Success:(SuccessBlock)success Error:(ErrorBlock)error;
#pragma mark - 锁定信息
+(void)getLockMessgaeWithDid:(NSString *)did Mid:(NSString *)mid Success:(SuccessBlock)success Error:(ErrorBlock)error;
#pragma mark - 解锁信息
+(void)getUnlockMessageWithDid:(NSString *)did Mid:(NSString *)mid Success:(SuccessBlock)success Error:(ErrorBlock)error;

/***************公共接口*******************/
#pragma mark - 获取省份接口  暂时没用
+ (void)getProvinceWithSuccess:(SuccessBlock)success Error:(ErrorBlock)error;
#pragma mark - 获取市级接口  暂时没用
+ (void)getCityByProvCodeWithCode:(NSString *)code Success:(SuccessBlock)success Error:(ErrorBlock)error;

#pragma mark - 获取县级接口  暂时没用
+ (void)getAreaByProvCodeWithCode:(NSString *)code Success:(SuccessBlock)success Error:(ErrorBlock)error;



#pragma mark - 获取一级分类
+ (void)getCateByClassWithDClass:(NSString *)dclass Group:(NSString *)group Success:(SuccessBlock)success Error:(ErrorBlock)error;
#pragma mark - 获取各个类别的多级分类
//顶级传  fatherId = 0
+ (void)getCategoryByFatherId:(NSString *)father Type:(NSString *)type Success:(SuccessBlock)success Error:(ErrorBlock)error;


#pragma mark - 根据id获取分类信息
+ (void)getCategoryByIdWithCateId:(NSString *)cateid uccess:(SuccessBlock)success Error:(ErrorBlock)error;


#pragma mark - 省市县统一接口
+ (void)getAllAreaListSuccess:(SuccessBlock)success Error:(ErrorBlock)error;
@end
