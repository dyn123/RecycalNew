//
//  Engine.m
//  RecycalNew
//
//  Created by apple on 2017/8/11.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "Engine.h"

#import "HttpRequestManager.h"
@implementation Engine

/*****************登录注册******************/
#pragma mark - 用户注册
+(void)postRegisterWithUserName:(NSString *)username  Password:(NSString *)password Mobile:(NSString *)mobile Company:(NSString *)company Contact:(NSString *)contact Prov:(NSString *)prov City:(NSString *)city Cate:(NSString *)cate Ip:(NSString *)Ip Success:(SuccessBlock)success Error:(ErrorBlock)error{
    username = @"130000";
    password = @"123456";
    mobile = @"12345678900";
    company = @"某某有限公司";
    contact = @"0311-11111111";
    prov = @"130000";
    city = @"130100";
    cate = @"2222";
    Ip = @"128.0.0.0"; //可忽略
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:username forKey:@"username"];
    [param setValue:password forKey:@"password"];
    [param setValue:mobile forKey:@"mobile"];
    [param setValue:company forKey:@"company"];
    [param setValue:contact forKey:@"contact"];
    [param setValue:prov forKey:@"prov"];
    [param setValue:city forKey:@"city"];
    [param setValue:cate forKey:@"cate"];
    [param setValue:Ip forKey:@"Ip"];
    
    
    
    
    
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/login/Register",BASE_URL];
    
    [HttpRequestManager Post:urlStr parameters:param Success:^(NSDictionary *dic) {
        
    } Error:^(NSError *error) {
        
    }];
    
    
}

#pragma mark - 用户登录
+(void)postLoginWithName:(NSString *)name Password:(NSString *)password Ip:(NSString *)ip Success:(SuccessBlock)success Error:(ErrorBlock)error{
    name = @"130000";
    password = @"123456";
    
    ip = @"128.0.0.0"; //可忽略
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:name forKey:@"name"];
    [param setValue:password forKey:@"password"];
  
    [param setValue:ip forKey:@"ip"];
    
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/login/API_UserLogin",BASE_URL];
    
    [HttpRequestManager Post:urlStr parameters:param Success:^(NSDictionary *dic) {
        
    } Error:^(NSError *error) {
        
    }];
    
}

#pragma mark - 发送验证码
+(void)getSendCodeWithPhoneNum:(NSString *)phoneNum Type:(NSString *)type Mid:(NSString *)mid Success:(SuccessBlock)success Error:(ErrorBlock)error{
    
    phoneNum = @"130000";
    type = @"1";
    mid = @"234";
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:phoneNum forKey:@"phoneNum"];
    [param setValue:type forKey:@"type"];
    
    [param setValue:mid forKey:@"mid"];
    
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/login/SendCode",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
}

#pragma mark - 刷新token
+(void)getRefreshLogTimeWithToken:(NSString *)token Success:(SuccessBlock)success Error:(ErrorBlock)error{
   
    token = @"36742368728389623";
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:token forKey:@"token"];
    
    
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/login/RefreshLogTime",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
}

#pragma mark - 验证验证码
+(void)getVerifyCodeWithPhoneNum:(NSString *)phoneNum  Type:(NSString *)type Code:(NSString *)code Success:(SuccessBlock)success Error:(ErrorBlock)error{
    
    phoneNum = @"11234561234";
    type = @"1";
    code = @"341256";
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:phoneNum forKey:@"phoneNum"];
    [param setValue:type forKey:@"type"];
    [param setValue:code forKey:@"code"];
    
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/login/GetVerifyCode",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
}
/*****************设置中心******************/
#pragma mark - 修改密码
+(void)postChangePWDWithMid:(NSString *)mid PwdStr1:(NSString *)pwdstr1 PwdStr2:(NSString *)pwdstr2 OldPwdStr:(NSString *)oldpwdstr Type:(NSString *)type Success:(SuccessBlock)success Error:(ErrorBlock)error{
    
    mid = @"130000";
    pwdstr1 = @"123456";
    pwdstr2 = @"123456";
    oldpwdstr = @"1234";
    type = @"2";
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:mid forKey:@"mid"];
    [param setValue:pwdstr1 forKey:@"pwdstr1"];
    [param setValue:pwdstr2 forKey:@"pwdstr2"];
    [param setValue:oldpwdstr forKey:@"oldpwdstr"];
    [param setValue:type forKey:@"type"];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/login/ChangePWD",BASE_URL];
    
    [HttpRequestManager Post:urlStr parameters:param Success:^(NSDictionary *dic) {
        
    } Error:^(NSError *error) {
        
    }];
}


/***************公共接口*******************/
#pragma mark - 获取省份接口
+ (void)getProvinceWithSuccess:(SuccessBlock)success Error:(ErrorBlock)error{
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/common/Province",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:nil Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
    
}
#pragma mark - 获取市级接口
+ (void)getCityByProvCodeWithCode:(NSString *)code Success:(SuccessBlock)success Error:(ErrorBlock)error{
    code = @"130000";
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:code forKey:@"code"];
    
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/common/CityByProvCode",BASE_URL];
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
}
#pragma mark - 获取县级接口
+ (void)getAreaByProvCodeWithCode:(NSString *)code Success:(SuccessBlock)success Error:(ErrorBlock)error{
    code = @"130100";
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:code forKey:@"code"];
    
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/common/area",BASE_URL];
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
}
#pragma mark - 获取多级分类
+ (void)getCateByClassWithDClass:(NSString *)dclass Group:(NSString *)group Success:(SuccessBlock)success Error:(ErrorBlock)error{
    
//    dclass = @"1";
//    group = @"2";
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:dclass forKey:@"dclass"];
    [param setValue:group forKey:@"group"];
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/common/GetCateByClass",BASE_URL];
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
}
#pragma mark - 获取各个类别的多级分类
  //顶级传  fatherId = 0
+ (void)getCategoryByFatherId:(NSString *)father Type:(NSString *)type Success:(SuccessBlock)success Error:(ErrorBlock)error{
    father = @"0";
    type = @"1";
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:father forKey:@"father"];
    [param setValue:type forKey:@"type"];
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/common/CategoryByFatherId",BASE_URL];
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
}
#pragma mark - 根据id获取分类信息
+ (void)getCategoryByIdWithCateId:(NSString *)cateid uccess:(SuccessBlock)success Error:(ErrorBlock)error{
    
    cateid = @"6795";
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:cateid forKey:@"cateid"];
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/common/CategoryById",BASE_URL];
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
    
}

#pragma mark - 省市县统一接口
+ (void)getAllAreaListSuccess:(SuccessBlock)success Error:(ErrorBlock)error{
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/common/GetAllAreaList",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:nil Success:^(NSDictionary *dic) {
        success(dic);
    } Error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
/*****************首页******************/
#pragma mark - 猜你喜欢
+(void)getGuessYouLikeWithCida:(NSString *)cida PageSize:(NSString *)pagesize Page:(NSString *)page Uid:(NSString *)uid  Success:(SuccessBlock)success Error:(ErrorBlock)error{
    
    cida = @"0";
    pagesize = @"10";
    page = @"1";
    uid = @"0";
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:cida forKey:@"cida"];
    [param setValue:pagesize forKey:@"pagesize"];
    [param setValue:page forKey:@"page"];
    [param setValue:uid forKey:@"uid"];
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/datainfo/GuessYouLike",BASE_URL];
   
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
    } Error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}

#pragma mark - 供求列表
+(void)getGQinfoListWithCateA:(NSString *)cateA CateB:(NSString *)cateB CateC:(NSString *)cateC Pcode:(NSString *)pcode Ccode:(NSString *)ccode Acode:(NSString *)acode Mlevel:(NSString *)mlevel Title:(NSString *)title  Pagesize:(NSString *)pagesize Pageindex:(NSString *)pageindex Group:(NSString *)group Success:(SuccessBlock)success Error:(ErrorBlock)error{
    
    cateA = @"0";
    cateB = @"0";
    cateC = @"0";
    pcode = @"130000";
    ccode = @"130100";
    acode = @"0";
    mlevel = @"0";
    title = @"";
    pagesize = @"8";
    pageindex = @"1";
    group = @"-1";
 
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:cateA forKey:@"cateA"];
    [param setValue:cateB forKey:@"cateB"];
    [param setValue:cateC forKey:@"cateC"];
    [param setValue:pcode forKey:@"pcode"];
    [param setValue:ccode forKey:@"ccode"];
    [param setValue:acode forKey:@"acode"];
    [param setValue:mlevel forKey:@"mlevel"];
    [param setValue:title forKey:@"title"];
    [param setValue:pagesize forKey:@"pagesize"];
    [param setValue:pageindex forKey:@"pageindex"];
    [param setValue:group forKey:@"group"];
    
    
//    NSString *para = [NSString stringWithFormat:@"?cateA=%@&cateB=%@&cateC=%@&pcode=%@&ccode=%@&acode=%@&mlevel=%@&title=%@&orderby=%@&pagesize=%@&pageindex=%@&group=%@",cateA,cateB,cateC,pcode,ccode,acode,mlevel,title,orderby,pagesize,pageindex,group];
    NSString * urlStr =[NSString stringWithFormat:@"%@api/datainfo/gqinfolist",BASE_URL];
//    urlStr = [urlStr stringByAppendingString:para];
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
       
    } Error:^(NSError *error) {
        
    }];
 
}



#pragma mark - 资产列表
+(void)getAssetInfoListWithCateA:(NSString *)cateA CateB:(NSString *)cateB CateC:(NSString *)cateC Pcode:(NSString *)pcode Ccode:(NSString *)ccode Acode:(NSString *)acode Mlevel:(NSString *)mlevel Sesrch:(NSString *)search Orderby:(NSString *)orderby Pagesize:(NSString *)pagesize Pageindex:(NSString *)pageindex Group:(NSString *)group Date:(NSString *)date Success:(SuccessBlock)success Error:(ErrorBlock)error{
    
    
    cateA = @"1";
    cateB = @"1";
    cateC = @"1";
    pcode = @"110000";
    ccode = @"110100";
    acode = @"110100";
    mlevel = @"0";
    search = @"";
    orderby = @"";
    pagesize = @"8";
    pageindex = @"1";
    group = @"0";
    date = @"30";
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:cateA forKey:@"cateA"];
    [param setValue:cateB forKey:@"cateB"];
    [param setValue:cateC forKey:@"cateC"];
    [param setValue:pcode forKey:@"pcode"];
    [param setValue:ccode forKey:@"ccode"];
    [param setValue:acode forKey:@"acode"];
    [param setValue:mlevel forKey:@"mlevel"];
    [param setValue:search forKey:@"search"];
    [param setValue:orderby forKey:@"orderby"];
    [param setValue:pagesize forKey:@"pagesize"];
    [param setValue:pageindex forKey:@"pageindex"];
    [param setValue:group forKey:@"group"];
    [param setValue:date forKey:@"date"];
    
    
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/datainfo/AssestinfoList",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];

    
    
}

#pragma mark - 拍卖列表
+(void)getAuctionInfoListWithCateA:(NSString *)cateA CateB:(NSString *)cateB CateC:(NSString *)cateC Pcode:(NSString *)pcode Ccode:(NSString *)ccode Acode:(NSString *)acode Mlevel:(NSString *)mlevel SesrchKey:(NSString *)searchKey Pagesize:(NSString *)pagesize Pageindex:(NSString *)pageindex Group:(NSString *)group Date:(NSString *)date HasContent:(NSString *)hasContent Success:(SuccessBlock)success Error:(ErrorBlock)error{
    
    cateA = @"1";
    cateB = @"1";
    cateC = @"1";
    pcode = @"110000";
    ccode = @"110100";
    acode = @"110100";
    mlevel = @"0";
    searchKey = @"";
    pagesize = @"8";
    pageindex = @"1";
    group = @"0";
    date = @"30";
    hasContent = @"0";
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:cateA forKey:@"cateA"];
    [param setValue:cateB forKey:@"cateB"];
    [param setValue:cateC forKey:@"cateC"];
    [param setValue:pcode forKey:@"pcode"];
    [param setValue:ccode forKey:@"ccode"];
    [param setValue:acode forKey:@"acode"];
    [param setValue:mlevel forKey:@"mlevel"];
    [param setValue:searchKey forKey:@"searchKey"];

    [param setValue:pagesize forKey:@"pagesize"];
    [param setValue:pageindex forKey:@"pageindex"];
    [param setValue:group forKey:@"group"];
    [param setValue:date forKey:@"date"];
    [param setValue:hasContent forKey:@"hasContent"];
    
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/datainfo/AuctioninfoList",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
    
}

#pragma mark - 租赁列表
+(void)getLeaseDataInfoListWithCateA:(NSString *)cateA CateB:(NSString *)cateB CateC:(NSString *)cateC Pcode:(NSString *)pcode Ccode:(NSString *)ccode Acode:(NSString *)acode InfoType:(NSString *)infotype Sesrch:(NSString *)search Pagesize:(NSString *)pagesize Pageindex:(NSString *)pageindex Success:(SuccessBlock)success Error:(ErrorBlock)error{
    cateA = @"1";
    cateB = @"1";
    cateC = @"1";
    pcode = @"110000";
    ccode = @"110100";
    acode = @"110100";
    infotype = @"1";
    search = @"";
    pagesize = @"8";
    pageindex = @"1";
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:cateA forKey:@"cateA"];
    [param setValue:cateB forKey:@"cateB"];
    [param setValue:cateC forKey:@"cateC"];
    [param setValue:pcode forKey:@"pcode"];
    [param setValue:ccode forKey:@"ccode"];
    [param setValue:acode forKey:@"acode"];
    [param setValue:infotype forKey:@"infotype"];
    [param setValue:search forKey:@"search"];
    
    [param setValue:pagesize forKey:@"pagesize"];
    [param setValue:pageindex forKey:@"pageindex"];
    
    
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/datainfo/LeaseDataInfoList",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
    
    
    
}

#pragma mark - 信息详情
+(void)getDataDetailModelWithDid:(NSString *)did DataType:(NSString *)datatype Uid:(NSString *)uid Success:(SuccessBlock)success Error:(ErrorBlock)error{
    
    did = @"1881";
    datatype = @"1";
    uid = @"757206";
   
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:did forKey:@"did"];
    [param setValue:datatype forKey:@"datatype"];
    [param setValue:uid forKey:@"uid"];
  
    
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/datainfo/DataDetailModel",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
    
}

/***************个人中心*******************/
#pragma mark - 获取会员信息
+(void)getMemeberById:(NSString *)Id uccess:(SuccessBlock)success Error:(ErrorBlock)error{
    Id = @"123";
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:Id forKey:@"id"];

    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/login/GetMemeberByid",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
}
#pragma mark - 浏览记录
+(void)getMemberBrowseLogWithDay:(NSString *)day Dclass:(NSString *)dclass Page:(NSString *)page Mid:(NSString *)mid PageSize:(NSString *)pagesize Success:(SuccessBlock)success Error:(ErrorBlock)error{
    day = @"7";
    dclass = @"1";
    page = @"1";
    mid = @"757206";
    pagesize = @"10";
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:day forKey:@"day"];
    [param setValue:dclass forKey:@"dclass"];
    [param setValue:page forKey:@"page"];
    [param setValue:mid forKey:@"mid"];
    [param setValue:pagesize forKey:@"pagesize"];
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/datainfo/GetMemeberBrowseLog",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
}
#pragma mark - 删除浏览记录
+(void)getDelBrowseLogWithDid:(NSString *)did Success:(SuccessBlock)success Error:(ErrorBlock)error{
    
    did = @"7";

    
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:did forKey:@"did"];

    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/datainfo/DelBrowseLog",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
    
    
}
#pragma mark - 收藏信息
+(void)getSetCollectWithDid:(NSString *)did Mid:(NSString *)mid Success:(SuccessBlock)success Error:(ErrorBlock)error{
    
    did = @"7";
    mid = @"1";
    
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:did forKey:@"did"];
    [param setValue:mid forKey:@"mid"];
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/datainfo/SetCollect",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
    
}
#pragma mark - 取消收藏
+(void)getUnCollectWithDid:(NSString *)did Mid:(NSString *)mid Success:(SuccessBlock)success Error:(ErrorBlock)error{
    
    did = @"7";
    mid = @"1";
    
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:did forKey:@"did"];
    [param setValue:mid forKey:@"mid"];
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/datainfo/UnCollect",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
}

#pragma mark - 我的推送
+(void)GetRecommendInfoListWithDataClass:(NSString *)dataclass PageIndex:(NSString *)pageindex PageSize:(NSString *)pagesize Mid:(NSString *)mid Title:(NSString *)title Success:(SuccessBlock)success Error:(ErrorBlock)error{
    
    dataclass = @"7";
    pageindex = @"1";
    pagesize = @"1";
    mid = @"1";
    title = @"1";
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:dataclass forKey:@"dataclass"];
    [param setValue:pageindex forKey:@"pageindex"];
    [param setValue:pagesize forKey:@"pagesize"];
    [param setValue:mid forKey:@"mid"];
    [param setValue:title forKey:@"title"];
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/datainfo/GetRecommendInfoList",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];

}
#pragma mark - 锁定信息
+(void)getLockMessgaeWithDid:(NSString *)did Mid:(NSString *)mid Success:(SuccessBlock)success Error:(ErrorBlock)error{
    
    did = @"7";
    mid = @"1";
  
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:did forKey:@"did"];
    [param setValue:mid forKey:@"mid"];
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/datainfo/LockMessgae",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
    
}
#pragma mark - 解锁信息
+(void)getUnlockMessageWithDid:(NSString *)did Mid:(NSString *)mid Success:(SuccessBlock)success Error:(ErrorBlock)error{
    did = @"7";
    mid = @"1";
    
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:did forKey:@"did"];
    [param setValue:mid forKey:@"mid"];
    
    NSString * urlStr =[NSString stringWithFormat:@"%@api/datainfo/UnlockMessage",BASE_URL];
    
    [HttpRequestManager Get:urlStr parameters:param Success:^(NSDictionary *dic) {
        success(dic);
        
    } Error:^(NSError *error) {
        
    }];
    
}




@end
