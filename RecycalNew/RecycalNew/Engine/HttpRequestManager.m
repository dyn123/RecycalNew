//
//  HttpRequestManager.m
//  RecycalNew
//
//  Created by apple on 2017/8/15.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "HttpRequestManager.h"

@implementation HttpRequestManager
+ (instancetype)httpRequest {
    return [[self class] alloc];
}
+ (void)setTimeOutWith:(AFHTTPSessionManager *)manager{
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10.f;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
}
#pragma mark - AFN
+ (void)Post:(NSString *)URLString parameters:(NSDictionary *)dict Success:(SuccessBlock)success Error:(ErrorBlock)error{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [self setTimeOutWith:manager];
    
    [manager POST:URLString parameters:dict progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
       
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
     

    }];
    
    
    
}

+ (void)Get:(NSString *)URLString parameters:(id)parameters Success:(SuccessBlock)success Error:(ErrorBlock)error{


    AFHTTPSessionManager * manager =[AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [self setTimeOutWith:manager];//设置超时
    
    
    [manager GET:URLString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        NSLog(@"process--%@",downloadProgress);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
        
    }];
   
    
}


@end
