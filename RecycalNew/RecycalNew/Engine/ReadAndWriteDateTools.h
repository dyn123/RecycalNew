//
//  ReadAndWriteDateTools.h
//  RecycalNew
//
//  Created by apple on 2017/8/23.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReadAndWriteDateTools : NSObject


//存储地区信息
- (void)writeAllArea;
//存储分类信息
- (void)writeAllCategory;


#pragma mark - 读取分类信息
- (NSMutableArray *)readCateWithFirstCate:(NSString *)firstCate SecondCate:(NSString *)secCate ThirCate:(NSString *)thirCate plistName:(NSString *)plist;
#pragma mark  读取地址信息
- (NSMutableArray *)readAllAreaWithProvCode:(NSString *)provCode CityCode:(NSString *)cityCode AreaCode:(NSString *)areaCode Plist:(NSString *)plist;


+ (ReadAndWriteDateTools *)tools;

@end
