//
//  HttpRequestManager.h
//  RecycalNew
//
//  Created by apple on 2017/8/15.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^SuccessBlock)(NSDictionary *dic);
typedef void (^ErrorBlock)(NSError *error);

@interface HttpRequestManager : NSObject

+ (instancetype)httpRequest;
+ (void)Post:(NSString *)URLString parameters:(NSDictionary *)dict Success:(SuccessBlock)success Error:(ErrorBlock)error;

+ (void)Get:(NSString *)URLString parameters:(id)parameters Success:(SuccessBlock)success Error:(ErrorBlock)error;

@end
