//
//  AuctioninfoListModel.h
//  RecycalNew
//
//  Created by apple on 2017/8/15.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuctionInfoListModel : NSObject
@property (nonatomic,copy)NSString *D_BeginTime;
@property (nonatomic,copy)NSString *D_Cid;
@property (nonatomic,copy)NSString *D_CidA;
@property (nonatomic,copy)NSString *D_CidB;
@property (nonatomic,copy)NSString *D_CidC;
@property (nonatomic,copy)NSString *D_CityCode;
@property (nonatomic,copy)NSString *D_Class;
@property (nonatomic,copy)NSString *D_Company;
@property (nonatomic,copy)NSString *D_Contact;
@property (nonatomic,copy)NSString *D_CountryCode;
@property (nonatomic,copy)NSString *D_EndTime;
@property (nonatomic,copy)NSString *D_Group;
@property (nonatomic,copy)NSString *D_Hits;
@property (nonatomic,copy)NSString *D_Id;
@property (nonatomic,copy)NSString *D_Introduce;
@property (nonatomic,copy)NSString *D_KeyWords;
@property (nonatomic,copy)NSString *D_M_Id;
@property (nonatomic,copy)NSString *D_Moblie;
@property (nonatomic,copy)NSString *D_MydataId;
@property (nonatomic,copy)NSString *D_Pic;
@property (nonatomic,copy)NSString *D_ProvinceCode;
@property (nonatomic,copy)NSString *D_PubTime;
@property (nonatomic,copy)NSString *D_Recommend;
@property (nonatomic,copy)NSString *D_RefreshTime;
@property (nonatomic,copy)NSString *D_State;
@property (nonatomic,copy)NSString *D_Status;
@property (nonatomic,copy)NSString *D_Title;
@property (nonatomic,copy)NSString *ExtAu_Address;
@property (nonatomic,copy)NSString *ExtAu_AuctionTime;
@property (nonatomic,copy)NSString *ExtAu_D_Id;
@property (nonatomic,copy)NSString *ExtAu_Deposit;
@property (nonatomic,copy)NSString *ExtAu_DepositUnit;
@property (nonatomic,copy)NSString *ExtAu_Description;
@property (nonatomic,copy)NSString *ExtAu_Id;
@property (nonatomic,copy)NSString *Id;

@end
