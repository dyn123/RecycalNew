//
//  GuessLikeModel.h
//  RecycalNew
//
//  Created by apple on 2017/8/23.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GuessLikeModel : NSObject
@property (nonatomic,strong)NSString *D_Title; //标题
@property (nonatomic,strong)NSString *D_PubTime;//发布日期
@property (nonatomic,strong)NSString *D_EndTime;//有限日期
@property (nonatomic,strong)NSString *D_ProvinceCode;//省
@property (nonatomic,strong)NSString *D_CityCode;//市
@property (nonatomic,strong)NSString *D_CidA;//第一级别分类
@property (nonatomic,strong)NSString *D_Class;//1-9
@property (nonatomic,strong)NSString *D_Group;//0 2
@end
